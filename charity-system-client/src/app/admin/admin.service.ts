import { Injectable } from '@angular/core';
import { Observable, throwError, observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AppConfig } from '../config/app.config';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  constructor(private http: HttpClient, private configService: AppConfig) {}

  addCampaign(body) {
    return this.http.post(this.configService.apiUrl + '/add-campaign', body);
  }

  addCenter(body) {
    return this.http.post(this.configService.apiUrl + '/add-center', body);
  }

  addItem(body) {
    return this.http.post(this.configService.apiUrl + '/add-item', body);
  }

  addFiles(images: File) {
    const arr = [];
    const formData = new FormData();
    arr.push(images);

    formData.append('title', 'opapappaap');

    arr[0].forEach((item, i) => {
      formData.append('estateImages', arr[0][i], arr[0][i].name);
    });

    return this.http.post<any>(
      this.configService.apiUrl + `/uploadImages`,
      formData
    );
  }

  deleteNotification(notificationId) {
    return this.http.post(this.configService.apiUrl + '/delete-notification', {
      notificationId
    });
  }

}
