import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  headElements = ['ID', 'Date', 'Name', 'Description', 'View More', 'Delete'];
  notificationsArr: any = [];

  constructor(
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.userService.getUserNotifications({userEmail: this.userService.getEmail()}).subscribe(
      (dataa: any) => {

        this.notificationsArr = dataa.userNotifications.map((el, index) => {
          return {
            id: index + 1,
            notName: el.notificationName.split(' ')[0],
            isReaded: el.notificationName.split(' ')[1],
            notDesc: el.notificationMessage,
            notDate: el.notificationDate.slice(0, 10),
            notId: el.notificationId
          }
        });
      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );
  }

  viewMore(reqNumber) {
    // console.log(reqNumber);
    this.userService.getRequestByNumber({reqNumber: reqNumber}).subscribe(
      (dataa: any) => {

        this.router.navigate([`requests/${dataa.requestId}`]);
      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );
    
  }

  delete(notId) {
    this.userService.deleteNotification({notId: notId}).subscribe(
      (dataa: any) => {

        this.notificationsArr = this.notificationsArr.filter(el=> el.notId !== notId);
        this.toastService.success('', 'Вие успешно изтрихте известие!');
      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );
  }

}
