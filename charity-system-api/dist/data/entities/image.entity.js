"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const residence_entity_1 = require("./residence.entity");
const land_entity_1 = require("./land.entity");
let Image = class Image {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Image.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Image.prototype, "fileName", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Image.prototype, "filePath", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Date)
], Image.prototype, "creationDate", void 0);
__decorate([
    typeorm_1.ManyToOne(type => residence_entity_1.Residence, residence => residence.residenceImages),
    __metadata("design:type", Promise)
], Image.prototype, "residenceImage", void 0);
__decorate([
    typeorm_1.ManyToOne(type => land_entity_1.Land, land => land.landImages),
    __metadata("design:type", Promise)
], Image.prototype, "landImage", void 0);
Image = __decorate([
    typeorm_1.Entity({
        name: 'images',
    })
], Image);
exports.Image = Image;
//# sourceMappingURL=image.entity.js.map