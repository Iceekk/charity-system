export declare class User {
    userId: string;
    email: string;
    password: string;
    role: string;
    dateOfPayment: Date;
    isLogged: boolean;
    isOkayWithTermsAndConditions: boolean;
    typeOfPayment: string;
    favouriteEstates: string;
}
