import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-add-campaign',
  templateUrl: './add-campaign.component.html',
  styleUrls: ['./add-campaign.component.css']
})
export class AddCampaignComponent implements OnInit {

  public landForm: FormGroup;
  fileArr = [];
  imgArr = [];
  fileObj = [];
  form: FormGroup;
  msg: string;
  progress = 0;
  landImages = [];

  // ===========================================
  dropdownCampaignStatus = [];
  dropdownCampaignStatusSettigns = {};

  selectedCampaignStatus: any;

  constructor(
    private toastService: ToastrService,
    private formbBuilder: FormBuilder,
    private adminService: AdminService,
    private sanitizer: DomSanitizer
  ) {
    this.form = this.formbBuilder.group({
      avatar: [null]
    });
  }

  ngOnInit() {
    this.dropdownCampaignStatus = [
      { item_id: 'OPEN', item_text: 'Отворена' },
      { item_id: 'CLOSED', item_text: 'Затворена' }
    ];
    this.dropdownCampaignStatusSettigns = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 3
    };

    this.buildTheForm();
  }

  onCampaignStatusSelect(item: any) {
  }

  successToast() {
    this.toastService.success('', 'Admin added!', { timeOut: 1000 });
  }

  errToast() {
    this.toastService.error('Please try again', 'Wrong input!', {
      timeOut: 1500
    });
  }

  getErrorMessage() {
    return '';
  }

  public buildTheForm() {
    this.landForm = this.formbBuilder.group({
      campaignName: this.formbBuilder.control(''),
      campaignDescription: this.formbBuilder.control(''),
      campaignStartDate: this.formbBuilder.control(''),
      campaignEndDate: this.formbBuilder.control(''),
      campaignPhone: this.formbBuilder.control(''),
      campaignEmail: this.formbBuilder.control('')
    });
  }

  get formData() {
    return this.landForm.value;
  }

  confirmAdd(addAdminData: any): void {
    const data = {
      moreData: addAdminData,
      campaignStatus: this.selectedCampaignStatus,
      landImages: this.landImages
    };

    this.selectedCampaignStatus = null;
    this.landForm.reset();
    this.fileArr = [];
    this.landImages = [];

    this.adminService.addCampaign(data).subscribe(
      dataa => {
        this.toastService.success('', 'Вие успешно добавихте нова кампания!');
        window.scrollTo(0, 0);
      },
      err => {
        this.toastService.error('', 'Възникна грешка при добавянето на нова кампания!');
      }
    );
  }

  upload(e: any) {
    const fileListAsArray = Array.from(e);
    fileListAsArray.forEach((item, i) => {
      const file = e as HTMLInputElement;
      const url = URL.createObjectURL(file[i]);
      this.imgArr.push(url);
      this.fileArr.push({ item, url });
    });

    this.fileArr.forEach(item => {
      this.fileObj.push(item.item);
    });

    // Set files form control
    this.form.patchValue({
      avatar: this.fileObj
    });

    this.form.get('avatar').updateValueAndValidity();

    // Upload to server
    this.adminService.addFiles(this.form.value.avatar).subscribe(
      data => {
        this.landImages = data.images;
      },
      error => {
      }
    );
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

}
