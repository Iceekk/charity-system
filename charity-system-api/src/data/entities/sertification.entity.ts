import { Image } from './image.entity';
import { User } from './user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity({
  name: 'sertifications',
})
export class Sertification {
  @PrimaryGeneratedColumn('uuid')
  sertificationId: string;

  @Column()
  sertificationName: string;

  @Column()
  sertificationDate: Date;

  @Column()
  sertificationCampaignId: string;

  @OneToOne(() => Image)
  @JoinColumn()
  sertificationImage: Image;

  @ManyToOne(type => User, user => user.userSertifications)
  user: Promise<User>;
}
