import { Column, PrimaryGeneratedColumn, Entity, OneToMany, ManyToMany, ManyToOne } from 'typeorm';
import { ItemCategory } from './../../models/enums/itemCategory.enum';
import { Campaign } from './campaign.entity';
import { Request } from './request.entity';
import { User } from './user.entity';

@Entity({
  name: 'items',
})
export class Item {
  @PrimaryGeneratedColumn('uuid')
  itemId: string;

  @Column()
  itemName: string;

  @Column()
  itemDescription: string;

  @Column({
    enum: [
      ItemCategory.CLOTHES,
      ItemCategory.TOYS,
      ItemCategory.FOOD,
      ItemCategory.BOOKS,
      ItemCategory.HOUSEHOLDNEEDS,
      ItemCategory.DEVICES
    ],
    type: 'enum'
  })
  itemCategory: string;

  @Column({ default: 0 })
  itemPoints: number;

  @Column({ default: 0 })
  itemQuantity: number;

  @ManyToOne(type => Request, request => request.requestItems)
  itemRequest: Promise<Request>;
}
