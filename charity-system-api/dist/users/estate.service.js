"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const company_entity_1 = require("../data/entities/company.entity");
const area_entity_1 = require("../data/entities/area.entity");
const Repository_1 = require("typeorm/repository/Repository");
const typeorm_1 = require("@nestjs/typeorm");
const common_1 = require("@nestjs/common");
const region_entity_1 = require("../data/entities/region.entity");
const user_entity_1 = require("../data/entities/user.entity");
const residence_entity_1 = require("./../data/entities/residence.entity");
const land_entity_1 = require("./../data/entities/land.entity");
const image_entity_1 = require("./../data/entities/image.entity");
const notification_entity_1 = require("./../data/entities/notification.entity");
let EstateService = class EstateService {
    constructor(regionRepository, companyRepository, areaRepository, usersRepository, residenceRepository, landRepository, imageRepository, notificationRepository) {
        this.regionRepository = regionRepository;
        this.companyRepository = companyRepository;
        this.areaRepository = areaRepository;
        this.usersRepository = usersRepository;
        this.residenceRepository = residenceRepository;
        this.landRepository = landRepository;
        this.imageRepository = imageRepository;
        this.notificationRepository = notificationRepository;
    }
    async editEstateForResidence() {
        return { info: 'no implemented' };
    }
    async removeEstateForResidence(data) {
        console.log(data);
        const residenceFound = await this.residenceRepository.findOne({ residenceId: `${data.data}` });
        const landFound = await this.landRepository.findOne({ landId: `${data.data}` });
        if (residenceFound) {
            await this.residenceRepository.delete(residenceFound);
            return { data: 'Successfully deleted!' };
        }
        if (landFound) {
            await this.landRepository.delete(landFound);
            return { data: 'Successfully deleted!' };
        }
        return { info: 'No estate was found!' };
    }
    async editEstateForLand() {
        return { info: 'no implemented' };
    }
    async removeEstateForLand() {
        return { info: 'no implemented' };
    }
    async getFilteredEstates(filters) {
        console.log(filters);
        return { info: 'no implemented' };
    }
    async getUnfilteredEstates(data) {
        const foundResidences = await this.residenceRepository.query(`SELECT
            *
        FROM
            residences AS r
                JOIN
            areas AS ar ON r.areaAreaId = ar.areaId
                JOIN
            regions AS re ON re.regionId = ar.regionRegionId
            JOIN
                images as img on img.residenceImageResidenceId = r.residenceId
                group by r.residenceId
                LIMIT 5;
            `);
        const foundLands = await this.landRepository.query(`SELECT
            *
        FROM
            lands AS l
                JOIN
            areas AS ar ON l.areaAreaId = ar.areaId
                JOIN
            regions AS re ON re.regionId = ar.regionRegionId
				JOIN
                images as img on img.landImageLandId = l.landId
                group by l.landId
                LIMIT 5;
            `);
        return { info: foundResidences.concat(foundLands) };
    }
    async getUnfilteredResidences(data) {
        const foundResidences = await this.residenceRepository.query(`SELECT
            *
        FROM
            residences AS r
                JOIN
            areas AS ar ON r.areaAreaId = ar.areaId
                JOIN
            regions AS re ON re.regionId = ar.regionRegionId
            JOIN
                images as img on img.residenceImageResidenceId = r.residenceId
                group by r.residenceId
                LIMIT 5;
            `);
        return { info: foundResidences };
    }
    async getUnfilteredLands(data) {
        const foundLands = await this.landRepository.query(`SELECT
            *
        FROM
            lands AS l
                JOIN
            areas AS ar ON l.areaAreaId = ar.areaId
                JOIN
            regions AS re ON re.regionId = ar.regionRegionId
				JOIN
                images as img on img.landImageLandId = l.landId
                group by l.landId
                LIMIT 5;
            `);
        return { info: foundLands };
    }
    async getRentEstatesCount() {
        const residencesRentCount = await this.residenceRepository.query(`SELECT
            COUNT(*) as rentsCount
        FROM residences as r
        WHERE r.residenceType = 'наеми';`);
        const landsRentCount = await this.landRepository.query(`SELECT
            COUNT(*) as rentsCount
        FROM lands as l
        WHERE l.landType = 'наеми';`);
        return { info: Number(residencesRentCount[0].rentsCount) + Number(landsRentCount[0].rentsCount) };
    }
    async getSaleEstatesCount() {
        const residencesSaleCount = await this.residenceRepository.query(`SELECT
            COUNT(*) as salesCount
        FROM residences as r
        WHERE r.residenceType = 'продажби';`);
        const landsSaleCount = await this.landRepository.query(`SELECT
            COUNT(*) as salesCount
        FROM lands as l
        WHERE l.landType = 'продажби';`);
        return { info: Number(residencesSaleCount[0].salesCount) + Number(landsSaleCount[0].salesCount) };
    }
    async getSpecificEstate(data) {
        const residenceFound = await this.residenceRepository.query(`SELECT
            res.residenceType as type,
            r.regionName as region,
            a.areaName,
            res.companyCompanyId
        FROM
            residences AS res
                JOIN
            areas AS a ON res.areaAreaId = a.areaId
                JOIN
            regions AS r ON r.regionId = a.regionRegionId
        WHERE
            res.residenceId = '${data.estateId}';`);
        const landFound = await this.landRepository.query(`SELECT
            l.landType as type,
            r.regionName as region,
            a.areaName,
            l.companyCompanyId
        FROM
            lands AS l
                JOIN
            areas AS a ON l.areaAreaId = a.areaId
                JOIN
            regions AS r ON r.regionId = a.regionRegionId
        WHERE
            l.landId = '${data.estateId}';`);
        if (!residenceFound && !landFound) {
            throw new common_1.HttpException('There is no such estate!', common_1.HttpStatus.NOT_FOUND);
        }
        if (residenceFound.length > 0) {
            const residenceInfo = await this.residenceRepository.find({ where: { residenceId: data.estateId } });
            const residenceCompany = await this.companyRepository.find({ where: { companyId: residenceFound[0].companyCompanyId } });
            return residenceFound.concat(residenceInfo).concat(residenceCompany);
        }
        if (landFound.length > 0) {
            const landInfo = await this.landRepository.find({ where: { landId: data.estateId } });
            const landCompany = await this.companyRepository.find({ where: { companyId: landFound[0].companyCompanyId } });
            return landFound.concat(landInfo).concat(landCompany);
        }
    }
};
EstateService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(region_entity_1.Region)),
    __param(1, typeorm_1.InjectRepository(company_entity_1.Company)),
    __param(2, typeorm_1.InjectRepository(area_entity_1.Area)),
    __param(3, typeorm_1.InjectRepository(user_entity_1.User)),
    __param(4, typeorm_1.InjectRepository(residence_entity_1.Residence)),
    __param(5, typeorm_1.InjectRepository(land_entity_1.Land)),
    __param(6, typeorm_1.InjectRepository(image_entity_1.Image)),
    __param(7, typeorm_1.InjectRepository(notification_entity_1.Notification)),
    __metadata("design:paramtypes", [Repository_1.Repository,
        Repository_1.Repository,
        Repository_1.Repository,
        Repository_1.Repository,
        Repository_1.Repository,
        Repository_1.Repository,
        Repository_1.Repository,
        Repository_1.Repository])
], EstateService);
exports.EstateService = EstateService;
//# sourceMappingURL=estate.service.js.map