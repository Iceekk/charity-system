import { JwtPayload } from '../../models/interfaces/jwt-payload';
import { ConfigService } from './../../config/config.service';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from './../auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { GetUserDTO } from '../../models/users/get-user.dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.jwtSecret,
    });
  }

  async validate(payload: JwtPayload): Promise<GetUserDTO> {
    const user = await this.authService.validateUser(payload);

    if (!user) {
      throw new HttpException('Not authorized', HttpStatus.UNAUTHORIZED);
    }
    return user;
  }
}
