import {
  Controller,
  Post,
  Body,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { UserRegisterDTO } from '../models/users/user-register.dto';
import { User } from '../data/entities/user.entity';
import { UserLoginDTO } from '../models/users/user-login.dto';
import { AuthService } from './auth.service';

@Controller('')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async sign(@Body() user: UserLoginDTO): Promise<object> {
    const generatedToken = await this.authService.signIn(user);
    if (!generatedToken) {
      throw new HttpException('Wrong credentials!', HttpStatus.NOT_FOUND);
    }

    return { token: generatedToken };
  }

  @Post('register/user')
  async registerUser(@Body() user: UserRegisterDTO): Promise<object> {
    return await this.authService.registerUser(user);
  }

  @Post('logOut')
  async logout(@Body() data: any): Promise<object> {
    return await this.authService.logout(data.userEmail);
  }
}
