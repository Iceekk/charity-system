import { UserService } from 'src/app/shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {

  public landForm: FormGroup;
  fileArr = [];
  imgArr = [];
  fileObj = [];
  form: FormGroup;
  msg: string;
  progress = 0;
  landImages = [];

  itemsArr: any = [];

  // elements: any = [
  //   {id: 1, first: 'Mark', last: 'Otto', handle: '@mdo'},
  //   {id: 2, first: 'Jacob', last: 'Thornton', handle: '@fat'},
  //   {id: 3, first: 'Larry', last: 'the Bird', handle: '@twitter'},
  // ];

  headElements = ['ID', 'Category', 'Name', 'Quantity', 'Description', 'Images', 'Delete'];

  //======================================
  dropdownCategory = [];
  dropdownCategorySettigns = {};

  selectedCategory: any;

  constructor(
    private toastService: ToastrService,
    private formbBuilder: FormBuilder,
    private adminService: AdminService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private userService: UserService,
  ) {
    this.form = this.formbBuilder.group({
      avatar: [null]
    });
  }

  ngOnInit() {
    this.dropdownCategory = [
      { item_id: 'CLOTHES', item_text: 'Дрехи' },
      { item_id: 'TOYS', item_text: 'Играчки' },
      { item_id: 'FOOD', item_text: 'Храна' },
      { item_id: 'BOOKS', item_text: 'Учебници и книги' },
      { item_id: 'HOUSEHOLDNEEDS', item_text: 'Домашни потребности' },
      { item_id: 'DEVICES', item_text: 'Уреди' }
    ];
    this.dropdownCategorySettigns = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 3
    };

    this.buildTheForm();
  }

  onCategorySelect(item: any) {
    // console.log(item);
  }

  successToast() {
    this.toastService.success('', 'Admin added!', { timeOut: 1000 });
  }

  errToast() {
    this.toastService.error('Please try again', 'Wrong input!', {
      timeOut: 1500
    });
  }

  getErrorMessage() {
    return '';
  }

  public buildTheForm() {
    this.landForm = this.formbBuilder.group({
      itemName: this.formbBuilder.control(''),
      itemQuantity: this.formbBuilder.control(''),
      itemDescription: this.formbBuilder.control('')
    });
  }

  get formData() {
    return this.landForm.value;
  }

  confirmAdd(addAdminData: any): void {
    const data = {
      moreData: addAdminData,
      selectedCategory: this.selectedCategory,
      landImages: this.landImages
    };
    
    this.itemsArr.push({
      id: this.itemsArr.length + 1,
      itemQuantity: addAdminData.itemQuantity,
      itemCat: this.selectedCategory[0].item_text,
      landImages: this.landImages,
      itemName: addAdminData.itemName,
      itemDesc: addAdminData.itemDescription,
    });

    this.selectedCategory = null;
    this.landForm.reset();
    this.fileArr = [];
    this.landImages = [];
  }

  deleteItem(itemName) {
    this.itemsArr = this.itemsArr.filter(el => el.itemName !== itemName);
  }

  confirmRequest() {
    const data = {
      reqNumber: Math.floor(Math.random() * 1000000000),
      reqDate: new Date(),
      reqStatus: 'В обработка',
      author: this.userService.getEmail(),
      reqItems: this.itemsArr,
    }

    this.itemsArr = [];

    this.userService.makeRequest(data).subscribe(
      dataa => {
        console.log(dataa);
        
        this.toastService.success('', 'Вие успешно направихте заявка!');
        window.scrollTo(0, 0);
      },
      err => {
        this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );
    
  }

  upload(e: any) {
    const fileListAsArray = Array.from(e);
    fileListAsArray.forEach((item, i) => {
      const file = e as HTMLInputElement;
      const url = URL.createObjectURL(file[i]);
      this.imgArr.push(url);
      this.fileArr.push({ item, url });
    });

    this.fileArr.forEach(item => {
      this.fileObj.push(item.item);
    });

    // Set files form control
    this.form.patchValue({
      avatar: this.fileObj
    });
    // console.log(fileListAsArray);
    // console.log();

    this.form.get('avatar').updateValueAndValidity();

    // Upload to server
    this.adminService.addFiles(this.form.value.avatar).subscribe(
      data => {
        // console.log(images);
        this.landImages = data.images;
      },
      error => {
        // console.log(error);
      }
    );
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

}
