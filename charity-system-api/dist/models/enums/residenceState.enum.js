"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ResidenceState;
(function (ResidenceState) {
    ResidenceState["FURNISHED"] = "\u043E\u0431\u0437\u0430\u0432\u0435\u0434\u0435\u043D";
    ResidenceState["UNFURNISHED"] = "\u043D\u0435\u043E\u0431\u0437\u0430\u0432\u0435\u0434\u0435\u043D";
})(ResidenceState = exports.ResidenceState || (exports.ResidenceState = {}));
//# sourceMappingURL=residenceState.enum.js.map