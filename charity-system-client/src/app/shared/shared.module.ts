import { AppConfig } from './../config/app.config';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { ServerErrorComponent } from './errors/server-error/server-error.component';
import { UnauthorisedComponent } from './errors/unauthorised/unauthorised.component';
import { HomeComponent } from './components/home/home.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { ClickCatcherDirective } from './components/home/clickCatcher.directive';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxGalleryModule } from 'ngx-gallery';
import { ProfileComponent } from './components/profile/profile.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { AwardsComponent } from './components/awards/awards.component';
import { CentersListComponent } from './components/centers-list/centers-list.component';
import { RankingComponent } from './components/ranking/ranking.component';
import { CampaignsListComponent } from './components/campaigns-list/campaigns-list.component';
import { CampaignComponent } from './components/campaign/campaign.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { MyrequestsComponent } from './components/myrequests/myrequests.component';
import { ViewrequestComponent } from './components/viewrequest/viewrequest.component';
import { ReviewrequestsComponent } from './components/reviewrequests/reviewrequests.component';
import { NotificationsComponent } from './components/notifications/notifications.component';

@NgModule({
    declarations: [
        NotFoundComponent,
        ServerErrorComponent,
        UnauthorisedComponent,
        HomeComponent,
        ClickCatcherDirective,
        AboutUsComponent,
        ProfileComponent,
        AwardsComponent,
        CentersListComponent,
        RankingComponent,
        CampaignsListComponent,
        CampaignComponent,
        UsersListComponent,
        StatisticsComponent,
        CalendarComponent,
        MyrequestsComponent,
        ViewrequestComponent,
        ReviewrequestsComponent,
        NotificationsComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        TooltipModule,
        MDBBootstrapModule.forRoot(),
        NgMultiSelectDropDownModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxGalleryModule,
        NgxPaginationModule,
        NgbModalModule,
        FlatpickrModule.forRoot(),
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [AppConfig],
})
export class SharedModule { }
