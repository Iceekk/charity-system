import { DatabaseType } from 'typeorm';
export interface EnvConfig {
    [key: string]: string;
}
export declare class ConfigService {
    private readonly envConfig;
    constructor(filePath?: string);
    private validateInput;
    readonly port: number;
    readonly jwtSecret: string;
    readonly jwtExpireTime: number;
    readonly dbHost: string;
    readonly dbPort: number;
    readonly dbUsername: string;
    readonly dbPassword: string;
    readonly dbName: string;
    readonly dbType: DatabaseType;
}
