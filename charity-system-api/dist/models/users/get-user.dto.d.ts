export declare class GetUserDTO {
    email: string;
    password: string;
    role: string;
}
