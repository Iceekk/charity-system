"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Construction;
(function (Construction) {
    Construction["PANEL"] = "\u043F\u0430\u043D\u0435\u043B";
    Construction["BRICK"] = "\u0442\u0443\u0445\u043B\u0430";
    Construction["EPK"] = "\u0415\u041F\u041A";
    Construction["TEMPORARY_BUILDING"] = "\u0432\u0440\u0435\u043C\u0435\u043D\u043D\u0430 \u043F\u043E\u0441\u0442\u0440\u043E\u0439\u043A\u0430";
    Construction["JOISTS"] = "\u0433\u0440\u0435\u0434\u043E\u0440\u0435\u0434";
    Construction["OTHER"] = "\u0434\u0440\u0443\u0433\u043E";
})(Construction = exports.Construction || (exports.Construction = {}));
//# sourceMappingURL=construction.enum.js.map