"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const users_service_1 = require("./users.service");
const path = require("path");
const multer_1 = require("multer");
const estate_service_1 = require("./estate.service");
const pngFileFilter = (req, file, callback) => {
    let ext = path.extname(file.originalname);
    if (ext !== '.png') {
        req.fileValidationError = 'Invalid file type';
        return callback(new Error('Invalid file type'), false);
    }
    return callback(null, true);
};
let UsersController = class UsersController {
    constructor(usersService, estateService) {
        this.usersService = usersService;
        this.estateService = estateService;
    }
    addResidence(body) {
        return this.usersService.addResidence(body);
    }
    addLand(body) {
        return this.usersService.addLand(body);
    }
    addAdmin() {
        return this.usersService.addAdmin();
    }
    async uploadImages(images, fileDto) {
        return { images };
    }
    fillDatabaseWithData() {
        return this.usersService.fillRegions();
    }
    getClientInfo() {
        return 'heii';
    }
    getNumberOfSales() {
        return this.estateService.getSaleEstatesCount();
    }
    getNumberOfRents() {
        return this.estateService.getRentEstatesCount();
    }
    getUnfilteredResidences(data) {
        return this.estateService.getUnfilteredResidences(data);
    }
    getUnfilteredLands(data) {
        return this.estateService.getUnfilteredLands(data);
    }
    getFilteredEstates(data) {
        return this.estateService.getFilteredEstates(data);
    }
    getReportedEstates() {
        return this.usersService.getNotifications();
    }
    reportEstate(data) {
        return this.usersService.createNotification(data);
    }
    deleteNotification(data) {
        return this.usersService.deleteNotification(data);
    }
    getSpecificEstate(data) {
        return this.estateService.getSpecificEstate(data);
    }
    getStatisticsData() {
        return this.usersService.getStatisticsData();
    }
    deleteEstate(data) {
        return this.estateService.removeEstateForResidence(data);
    }
};
__decorate([
    common_1.Post('add-residence'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "addResidence", null);
__decorate([
    common_1.Post('add-land'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "addLand", null);
__decorate([
    common_1.Get('add-admin'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "addAdmin", null);
__decorate([
    common_1.Post('uploadImages'),
    common_1.UseInterceptors(common_1.FilesInterceptor('estateImages', 999, {
        storage: multer_1.diskStorage({
            destination: './public/estates',
            filename: (req, file, cb) => {
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
                return cb(null, `${randomName}${path.extname(file.originalname)}`);
            },
        }),
    })),
    __param(0, common_1.UploadedFiles()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "uploadImages", null);
__decorate([
    common_1.Get('fill-regions'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "fillDatabaseWithData", null);
__decorate([
    common_1.Get('test'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Object)
], UsersController.prototype, "getClientInfo", null);
__decorate([
    common_1.Get('get-number-of-sales'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getNumberOfSales", null);
__decorate([
    common_1.Get('get-number-of-rents'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getNumberOfRents", null);
__decorate([
    common_1.Post('get-unfiltered-residences'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getUnfilteredResidences", null);
__decorate([
    common_1.Post('get-unfiltered-lands'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getUnfilteredLands", null);
__decorate([
    common_1.Post('get-filtered-estates'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getFilteredEstates", null);
__decorate([
    common_1.Get('get-reported-estates'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getReportedEstates", null);
__decorate([
    common_1.Post('report-estate'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "reportEstate", null);
__decorate([
    common_1.Post('delete-notification'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "deleteNotification", null);
__decorate([
    common_1.Post('get-specific-estate'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getSpecificEstate", null);
__decorate([
    common_1.Get('get-statistics'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getStatisticsData", null);
__decorate([
    common_1.Post('delete-estate'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "deleteEstate", null);
UsersController = __decorate([
    common_1.Controller(''),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        estate_service_1.EstateService])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=users.controller.js.map