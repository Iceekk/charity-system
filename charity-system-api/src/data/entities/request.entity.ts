import { Region } from './region.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Center } from './center.entity';
import { User } from './user.entity';
import { Item } from './item.entity';

@Entity({
  name: 'request',
})
export class Request {
  @PrimaryGeneratedColumn('uuid')
  requestId: string;

  @Column()
  requestNumber: string;

  @Column()
  requestDate: Date;

  @Column()
  requestStatus: string;

  @Column()
  requestCampaignID: string;

  @ManyToOne(type => User, user => user.userRequests)
  requestAuthor: Promise<User>;

  @OneToMany(type => Item, item => item.itemRequest, { eager: true })
  requestItems: Promise<Item[]>;
}
