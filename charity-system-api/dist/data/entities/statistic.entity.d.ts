export declare class Statistic {
    statisticId: string;
    statisticDate: Date;
    numberOfVisitors: number;
    numberOfRegisteredVisitors: number;
    createdEstates: number;
    deletedEstates: number;
}
