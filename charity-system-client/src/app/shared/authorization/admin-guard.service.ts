import { Injectable } from '@angular/core';
import { CanActivate, Router, } from '@angular/router';
import { UserService } from '../services/user.service';
import { Role } from '../models/enums/roles.enum';

@Injectable()
export class AdminGuard implements CanActivate {

    constructor(
        public userService: UserService,
        public router: Router
    ) {}

    canActivate(): boolean {
        if (Role.ADMIN === this.userService.getRole()) {
            return true;
        }

        this.router.navigate(['unauthorised']);
        return false;
    }
}
