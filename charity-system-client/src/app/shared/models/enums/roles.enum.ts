export enum Role {
    ADMIN = 'ADMIN',
    USER = 'USER',
    PAID_USER_3 = 'PAID_USER_3',
    PAID_USER_6 = 'PAID_USER_6',
    PAID_USER_12 = 'PAID_USER_12',
}
