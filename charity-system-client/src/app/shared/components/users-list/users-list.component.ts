import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MdbTableDirective } from 'angular-bootstrap-md';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  users: any;
  allUsers: any;
  
  constructor(
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { }

  @HostListener('input')
  oninput() {
    // this.searchItems();
  }

  ngOnInit() {
    this.userService.getAllUsers().subscribe((data) => {

      this.allUsers = data;
      this.users = data;
    }, (err) => {
    });
  }

  searchUser(){
    const searchedUserName = (document.getElementById('search-value') as HTMLInputElement).value.toString().trim();

    if (searchedUserName.length !== 0) {
      const users = this.users.filter((user)=> {
        if (user.firstName.toLowerCase().includes(searchedUserName)) return user;
      });
      if (users.length > 0) {
        this.users = users;
      }
    } else {
      this.users = this.allUsers
    }
  }

}
