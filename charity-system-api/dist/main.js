"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_service_1 = require("./config/config.service");
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const path_1 = require("path");
const cors = require("cors");
const common_1 = require("@nestjs/common");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.use(cors());
    app.useStaticAssets(path_1.join(__dirname, '..', 'public'));
    app.useGlobalPipes(new common_1.ValidationPipe({
        transform: true,
        whitelist: true,
    }));
    app.enableCors();
    await app.listen(app.get(config_service_1.ConfigService).port);
}
bootstrap();
//# sourceMappingURL=main.js.map