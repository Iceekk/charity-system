"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Role;
(function (Role) {
    Role["ADMIN"] = "ADMIN";
    Role["USER"] = "USER";
    Role["PAID_USER_3"] = "PAID_USER_3";
    Role["PAID_USER_6"] = "PAID_USER_6";
    Role["PAID_USER_12"] = "PAID_USER_12";
})(Role = exports.Role || (exports.Role = {}));
//# sourceMappingURL=roles.enum.js.map