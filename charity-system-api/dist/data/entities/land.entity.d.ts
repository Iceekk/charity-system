import { Area } from './area.entity';
import { Company } from './company.entity';
import { Image } from './image.entity';
export declare class Land {
    landId: string;
    landName: string;
    landDescription: string;
    landType: string;
    landNeighborhood: string;
    landPrice: number;
    landQuadrature: number;
    landViews: number;
    reportsCounter: number;
    currencyType: string;
    landEstateType: string;
    landFeatures: string;
    isActive: boolean;
    dateOfCreation: Date;
    area: Promise<Area>;
    company: Promise<Company>;
    landImages: Image[];
}
