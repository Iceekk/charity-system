import { Component, OnInit, AfterViewInit } from '@angular/core';
import {
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import { FlatpickrDefaultsInterface } from 'angularx-flatpickr/flatpickr-defaults.service';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'app-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  filtersLoaded: Promise<boolean>;

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  public datePickerOptions : FlatpickrDefaultsInterface = {
    enable : [{from : new Date(0, 1), to : new Date(new Date().getFullYear() + 200, 12)}]
  }

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter((iEvent) => iEvent !== event);
        this.handleEvent('Deleted', event);
      },
    },
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    {
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'A 3 day event',
      color: colors.red,
      actions: this.actions,
      allDay: true,
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
    },
    {
      start: startOfDay(new Date()),
      title: 'An event with no end date',
      color: colors.yellow,
      actions: this.actions,
    },
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'A long event that spans 2 months',
      color: colors.blue,
      allDay: true,
    },
    {
      start: addHours(startOfDay(new Date()), 2),
      end: addHours(new Date(), 2),
      title: 'A draggable and resizable event',
      color: colors.yellow,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
    },
  ];

  activeDayIsOpen: boolean = true;
  isAdmin: boolean = false;

  constructor(
    private modal: NgbModal,
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute,
  ) {}

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {

    // const newEvent = {
    //   regionId: "София",
    //   title: "New event" + this.events.length + (Math.random() * 10),
    //   start: startOfDay(new Date()),
    //   end: endOfDay(new Date()),
    //   color: colors.red,
    //   eventPhoneNumber: this.userService.getEmail(),
    // };

    // this.userService.createEvent(newEvent).subscribe(
    //   (dataa: any) => {
    //     // console.log(dataa);

        // this.events = [
        //   ...this.events, {
        //     title: 'New event' + this.events.length + (Math.random() * 10),
        //     start: startOfDay(new Date()),
        //     end: endOfDay(new Date()),
        //     color: colors.red,
        //     draggable: true,
        //     resizable: {
        //       beforeStart: true,
        //       afterEnd: true,
        //     },
        //   }
        // ];
        // this.testAdd();
        // this.toastService.success('', 'Вие успешно редактирахте заявката!');
        // this.router.navigate([`review-requests`]);
        // this.userRequests = dataa.userRequests;
        // window.scrollTo(0, 0);
        // this.filtersLoaded = Promise.resolve(true);
      // },
      // err => {
      //   // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      // }
    //);

    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true,
        },
      },
    ];

    // console.log(this.events);
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter((event) => event !== eventToDelete);

    // console.log(eventToDelete.title);
    
    // this.userService.deleteEvent({eventName: eventToDelete.title}).subscribe(
    //   (dataa: any) => {
    //     // console.log(eventToDelete.title);
        
    //     this.events = this.events.filter((event) => event.title !== eventToDelete.title);
    //     // console.log(this.events);
    //     this.toastService.success('', 'Вие успешно изтрихте резервираната дата!');
    //     // this.router.navigate([`review-requests`]);
    //     // this.userRequests = dataa.userRequests;
    //     // window.scrollTo(0, 0);
    //     // this.filtersLoaded = Promise.resolve(true);
    //   },
    //   err => {
    //     // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
    //   }
    // );
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  ngOnInit() {
    this.isAdmin = this.userService.getRole() === 'ADMIN' ? true : false;

    // this.userService.getUserEvents({userEmail: this.userService.getEmail()}).subscribe(
    //   (dataa: any) => {
    //     // console.log(dataa);


    //     dataa.forEach((el) => {
    //       this.add(el);
    //     });

    //     this.filtersLoaded = Promise.resolve(true);
    //     // console.log(this.events);
        
    //     // this.addEvent();
        
    //     //   ...this.events, {
    //     //     title: 'New event',
    //     //     start: startOfDay(new Date()),
    //     //     end: endOfDay(new Date()),
    //     //     color: colors.red,
    //     //     draggable: true,
    //     //     resizable: {
    //     //       beforeStart: true,
    //     //       afterEnd: true,
    //     //     },
    //     //   }
    //     // ];
    //     // this.toastService.success('', 'Вие успешно редактирахте заявката!');
    //     // this.router.navigate([`review-requests`]);
    //     // this.userRequests = dataa.userRequests;
    //     // window.scrollTo(0, 0);
    //     // this.filtersLoaded = Promise.resolve(true);
    //   },
    //   err => {
    //     // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
    //   }
    // );
  }

  testAdd() {
    this.events = [
        ...this.events,
        {
          title: 'New event',
          start: startOfDay(new Date()),
          end: endOfDay(new Date()),
          color: colors.red,
          draggable: true,
          resizable: {
            beforeStart: true,
            afterEnd: true,
          },
        },
      ];

      this.events = this.events.slice(0, -1);
  }

  add(el) {
    this.events = [
      ...this.events, 
      {
        title: el.eventName,
        start: addHours(startOfDay(Date.parse(el.eventStart)), 0),
        end: addHours(endOfDay(Date.parse(el.eventEnd)), 0),
        color: {primary: el.eventColor, secondary: el.eventColor},
        resizable: {
          beforeStart: true,
          afterEnd: true,
        },
        actions: this.actions,
        draggable: true,
    }];
  }

  confirm() {
    // console.log(this.events);

    this.toastService.success('', 'Вие успешно запазихте промените!');
    // this.userService.updateEvents({events: this.events}).subscribe(
    //   (dataa: any) => {
    //     // console.log(dataa);

    //     this.toastService.success('', 'Вие успешно запазихте промените!');
        
    //   },
    //   err => {
    //     // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
    //   }
    // );

  }

}
