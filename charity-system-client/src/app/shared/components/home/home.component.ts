import {
  Component,
  OnInit,
  ElementRef,
} from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  numberOfSales = 0;
  numberOfRents = 0;

  regions = [
    {
      id: 'first',
      title: 'Моля изберете град или област...',
      isSelected: true
    },
    {
      id: 'BG-01-point',
      title: 'Благоевград'
    },
    {
      id: 'BG-01',
      title: 'Благоевград област'
    },
    {
      id: 'BG-02-point',
      title: 'Бургас'
    },
    {
      id: 'BG-02',
      title: 'Бургас област'
    },
    {
      id: 'BG-03-point',
      title: 'Варна'
    },
    {
      id: 'BG-03',
      title: 'Варна област'
    },
    {
      id: 'BG-04-point',
      title: 'Велико Търново'
    },
    {
      id: 'BG-04',
      title: 'Велико Търново област'
    },
    {
      id: 'BG-05-point',
      title: 'Видин'
    },
    {
      id: 'BG-05',
      title: 'Видин област'
    },
    {
      id: 'BG-06-point',
      title: 'Враца'
    },
    {
      id: 'BG-06',
      title: 'Враца област'
    },
    {
      id: 'BG-07-point',
      title: 'Габрово'
    },
    {
      id: 'BG-07',
      title: 'Габрово област'
    },
    {
      id: 'BG-08-point',
      title: 'Добрич'
    },
    {
      id: 'BG-08',
      title: 'Добрич област'
    },
    {
      id: 'BG-09-point',
      title: 'Кърджали'
    },
    {
      id: 'BG-09',
      title: 'Кърджали област'
    },
    {
      id: 'BG-10-point',
      title: 'Кюстендил'
    },
    {
      id: 'BG-10',
      title: 'Кюстендил област'
    },
    {
      id: 'BG-11-point',
      title: 'Ловеч'
    },
    {
      id: 'BG-11',
      title: 'Ловеч област'
    },
    {
      id: 'BG-12-point',
      title: 'Монтана'
    },
    {
      id: 'BG-12',
      title: 'Монтана област'
    },
    {
      id: 'BG-13-point',
      title: 'Пазарджик'
    },
    {
      id: 'BG-13',
      title: 'Пазарджик област'
    },
    {
      id: 'BG-14-point',
      title: 'Перник'
    },
    {
      id: 'BG-14',
      title: 'Перник област'
    },
    {
      id: 'BG-15-point',
      title: 'Плевен'
    },
    {
      id: 'BG-15',
      title: 'Плевен област'
    },
    {
      id: 'BG-16-point',
      title: 'Пловдив'
    },
    {
      id: 'BG-16',
      title: 'Пловдив област'
    },
    {
      id: 'BG-17-point',
      title: 'Разград'
    },
    {
      id: 'BG-17',
      title: 'Разград област'
    },
    {
      id: 'BG-18-point',
      title: 'Русе'
    },
    {
      id: 'BG-18',
      title: 'Русе област'
    },
    {
      id: 'BG-19-point',
      title: 'Силистра'
    },
    {
      id: 'BG-19',
      title: 'Силистра област'
    },
    {
      id: 'BG-20-point',
      title: 'Сливен'
    },
    {
      id: 'BG-20',
      title: 'Сливен област'
    },
    {
      id: 'BG-21-point',
      title: 'Смолян'
    },
    {
      id: 'BG-21',
      title: 'Смолян област'
    },
    {
      id: 'BG-23',
      title: 'София'
    },
    {
      id: 'BG-22',
      title: 'София област'
    },
    {
      id: 'BG-24-point',
      title: 'Стара Загора'
    },
    {
      id: 'BG-24',
      title: 'Стара Загора област'
    },
    {
      id: 'BG-25-point',
      title: 'Търговище'
    },
    {
      id: 'BG-25',
      title: 'Търговище област'
    },
    {
      id: 'BG-26-point',
      title: 'Хасково'
    },
    {
      id: 'BG-26',
      title: 'Хасково област'
    },
    {
      id: 'BG-27-point',
      title: 'Шумен'
    },
    {
      id: 'BG-27',
      title: 'Шумен област'
    },
    {
      id: 'BG-28-point',
      title: 'Ямбол'
    },
    {
      id: 'BG-28',
      title: 'Ямбол област'
    }
  ];

  currentSelectedRegion = 'BG-23';

  constructor(private elemRef: ElementRef, private userService: UserService) {}

  ngOnInit() {}

  updateProperty($event) {
    this.currentSelectedRegion = $event;
    // document.getElementById($event.toString()).style
  }

  updateRegion(currentSelectedRegion: any) {
    const selectedRegion = this.regions.find(
      elem => elem.title === currentSelectedRegion
    );
    // console.log(selectedRegion);
    if (this.currentSelectedRegion.toString().indexOf('point') === -1) {
      (document.getElementById(
        this.currentSelectedRegion
      ) as HTMLElement).style.fill = '#1495ba';
    } else {
      (document.getElementById(
        this.currentSelectedRegion
      ) as HTMLElement).style.fill = 'white';
    }
    this.currentSelectedRegion = selectedRegion.id;
    (document.getElementById(
      this.currentSelectedRegion
    ) as HTMLElement).style.fill = '#ba14a4';
  }
}
