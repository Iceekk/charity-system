import { JwtService } from '@nestjs/jwt';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { JwtPayload } from '../models/interfaces/jwt-payload';
import { GetUserDTO } from '../models/users/get-user.dto';
import { UserLoginDTO } from '../models/users/user-login.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Role } from '../models/enums/roles.enum';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,

    private readonly jwtService: JwtService,
  ) {}

  public async signIn(user: UserLoginDTO): Promise<string> {
    const userFound = await this.usersRepository.findOne({
      select: ['userId', 'email', 'password', 'role'],
      where: { email: user.email },
    });

    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        if (userFound.isLogged) {
          return null;
        }

        const resultt = await this.usersRepository.update(userFound.userId, {
          isLogged: true,
        });

        return this.jwtService.sign({
          email: userFound.email,
          role: userFound.role,
        });
      }
    }
    return null;
  }

  async validateUser(payload: JwtPayload): Promise<GetUserDTO> {
    return await this.usersRepository.findOne({
      where: { email: payload.email },
    });
  }

  async registerUser(user) {
    const userFound = await this.usersRepository.findOne({
      where: { email: user.email },
    });

    if (userFound) {
      throw new HttpException('User already exists!', HttpStatus.FORBIDDEN);
    }

    user.password = await bcrypt.hash(user.password, 10);
    user.role = Role.USER;
    user.isOkayWithTermsAndConditions = user.isOkayWithTermsAndConditions;
    user.favouriteEstates = '';
    const createUser = await this.usersRepository.create(user);
    const result = await this.usersRepository.save(createUser);

    return { info: result};
  }

  async logout(userEmail: any): Promise<object> {
    const userFound = await this.usersRepository.findOne({
      where: { email: userEmail },
    });

    if (!userFound) {
      throw new HttpException('No user found!', HttpStatus.NOT_FOUND);
    }

    await this.usersRepository.update(userFound.userId, { isLogged: false });

    return { info: 'User status was updated successfully!' };
  }
}
