import { UsersService } from './users.service';
import { EstateService } from './estate.service';
import { Notification } from './../data/entities/notification.entity';
import { Statistic } from './../data/entities/statistic.entity';
export declare class UsersController {
    private readonly usersService;
    private readonly estateService;
    constructor(usersService: UsersService, estateService: EstateService);
    addResidence(body: any): Promise<object>;
    addLand(body: any): Promise<object>;
    addAdmin(): Promise<string>;
    uploadImages(images: any, fileDto: any): Promise<object>;
    fillDatabaseWithData(): Promise<string>;
    getClientInfo(): any;
    getNumberOfSales(): Promise<object>;
    getNumberOfRents(): Promise<object>;
    getUnfilteredResidences(data: any): Promise<object>;
    getUnfilteredLands(data: any): Promise<object>;
    getFilteredEstates(data: any): Promise<object>;
    getReportedEstates(): Promise<Notification[]>;
    reportEstate(data: any): Promise<object>;
    deleteNotification(data: any): Promise<object>;
    getSpecificEstate(data: any): Promise<object>;
    getStatisticsData(): Promise<Statistic[]>;
    deleteEstate(data: any): Promise<object>;
}
