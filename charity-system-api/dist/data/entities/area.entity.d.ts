import { Region } from './region.entity';
import { Residence } from './residence.entity';
import { Land } from './land.entity';
export declare class Area {
    areaId: string;
    areaName: string;
    region: Promise<Region>;
    areaResidences: Promise<Residence[]>;
    areaLands: Promise<Land[]>;
}
