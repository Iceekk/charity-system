"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CurrencyType;
(function (CurrencyType) {
    CurrencyType["BGN"] = "BGN";
    CurrencyType["EUR"] = "EUR";
})(CurrencyType = exports.CurrencyType || (exports.CurrencyType = {}));
//# sourceMappingURL=currencyType.enum.js.map