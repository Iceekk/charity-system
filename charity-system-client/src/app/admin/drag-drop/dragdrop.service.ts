import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from './../../config/app.config';

@Injectable({
  providedIn: 'root'
})
export class DragdropService {
  constructor(private http: HttpClient, private configService: AppConfig) {}

  addFiles(images: File) {
    const arr = [];
    const formData = new FormData();
    arr.push(images);

    formData.append('title', 'opapappaap');

    arr[0].forEach((item, i) => {
      formData.append('estateImages', arr[0][i], arr[0][i].name);
    });

    return this.http.post<any>(
      this.configService.apiUrl + '/uploadImages',
      formData
    );
  }
}
