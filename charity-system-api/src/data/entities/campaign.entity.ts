import { Column, PrimaryGeneratedColumn, Entity, OneToMany, ManyToMany, JoinTable, JoinColumn } from 'typeorm';
import { CampaignStatus } from './../../models/enums/campaignStatus.enum';
import { Item } from './item.entity';
import { Image } from './image.entity';
import { User } from './user.entity';

@Entity({
  name: 'campaigns',
})
export class Campaign {
  @PrimaryGeneratedColumn('uuid')
  campaignId: string;

  @Column()
  campaignName: string;

  @Column()
  campaignDescription: string;

  @Column()
  campaignStartDate: Date;

  @Column()
  campaignEndDate: Date;

  @Column({ default: 0 })
  campaignCollectedMoney: number;

  @Column()
  phoneForContacts: string;

  @Column()
  emailForContacts: string;

  @Column({
    enum: [
      CampaignStatus.OPEN,
      CampaignStatus.CLOSED
    ],
    type: 'enum',
    default: CampaignStatus.OPEN,
  })
  campaignStatus: string;

  @Column({ type: 'text' })
  campaignWinners: string;

  // @ManyToMany(() => Item)
  // @JoinTable()
  // campaignCollectedItems: Item[];

  @ManyToMany(() => User)
  @JoinTable()
  campaignUsers: User[];

  @OneToMany((type) => Image, image => image.campaignImage)
  campaignImages: Image[];
}
