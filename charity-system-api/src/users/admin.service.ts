import { Area } from '../data/entities/area.entity';
import { Repository } from 'typeorm/repository/Repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Region } from '../data/entities/region.entity';
import { User } from '../data/entities/user.entity';
import { Image } from '../data/entities/image.entity';
import { Campaign } from '../data/entities/campaign.entity';
import { Center } from '../data/entities/center.entity';
import { Item } from '../data/entities/item.entity';
import { Sertification } from '../data/entities/sertification.entity';

@Injectable()
export class AdminService {
  constructor(
    @InjectRepository(Region)
    private readonly regionRepository: Repository<Region>,
    @InjectRepository(Campaign)
    private readonly campaignsRepository: Repository<Campaign>,
    @InjectRepository(Area) 
    private readonly areaRepository: Repository<Area>,
    @InjectRepository(User) 
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Sertification)
    private readonly sertificationsRepository: Repository<Sertification>,
    @InjectRepository(Item) 
    private readonly itemsRepository: Repository<Item>,
    @InjectRepository(Image)
    private readonly imageRepository: Repository<Image>,
    @InjectRepository(Center)
    private readonly centersRepository: Repository<Center>
  ) {}

}

// npm run migration:generate -- --name=Initial
// npm run migration:run

// ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'YourRootPassword';
// -- or
// CREATE USER 'foo'@'%' IDENTIFIED WITH mysql_native_password BY 'bar';
// -- then
// FLUSH PRIVILEGES;
