"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
exports.UserRoles = common_1.createParamDecorator((data, req) => {
    return data ? req.user[data] : req.user.roles;
});
//# sourceMappingURL=user-roles.decorator.js.map