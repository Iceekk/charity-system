import { Residence } from './residence.entity';
import { Land } from './land.entity';
export declare class Company {
    companyId: string;
    companyName: string;
    companyPhone: string;
    companyEmail: string;
    companySiteURL: string;
    companyAddress: string;
    companyResidences: Promise<Residence[]>;
    companyLands: Promise<Land[]>;
}
