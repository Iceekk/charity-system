import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-reviewrequests',
  templateUrl: './reviewrequests.component.html',
  styleUrls: ['./reviewrequests.component.css']
})
export class ReviewrequestsComponent implements OnInit {

  isAuth: any = false;
  userRequests = [];
  filtersLoaded: Promise<boolean>;

  constructor(
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.isAuth = this.userService.getToken() ? true : false;

    this.userService.getAllPendingRequests().subscribe(
      (dataa: any) => {
        this.userRequests = dataa.map(el => {
          el.requestDate = el.requestDate.substring(0,10);
          return el;
        });
        this.filtersLoaded = Promise.resolve(true);
      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );
  }

  viewReq(reqId) {
    this.router.navigate([`requests/${reqId}`]);
  }

}
