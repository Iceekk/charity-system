import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-campaigns-list',
  templateUrl: './campaigns-list.component.html',
  styleUrls: ['./campaigns-list.component.css']
})
export class CampaignsListComponent implements OnInit {

  isAdmin: any = false;
  activeCampaign: any = {};
  campaigns: any;

  constructor(
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.isAdmin = this.userService.getRole() === 'ADMIN' ? true : false;

    this.userService.getCampaignByStatus().subscribe((data) => {

      this.activeCampaign = data;
    }, (err) => {
      // this.toastService.error('', 'Вече има съществуващ потребител с въведените данни!');
    });
    
    this.userService.getAllCampaigns().subscribe((data) => {

      this.campaigns = data;
    }, (err) => {
      // this.toastService.error('', 'Вече има съществуващ потребител с въведените данни!');
    });
  }

  confirmAdd() {
    this.router.navigate(['admin/add-campaign']);
  }

}
