import { AuthModule } from './../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { User } from './../data/entities/user.entity';
import { UsersController } from './users.controller';
import { Region } from '../data/entities/region.entity';
import { Area } from '../data/entities/area.entity';
import { UsersService } from './users.service';
import { AdminService } from './admin.service';
import { Image } from '../data/entities/image.entity';
import { Center } from './../data/entities/center.entity';
import { Campaign } from './../data/entities/campaign.entity';
import { Item } from './../data/entities/item.entity';
import { Sertification } from './../data/entities/sertification.entity';
import { Request } from './../data/entities/request.entity';
import { Notification } from './../data/entities/notification.entity';
import { Event } from './../data/entities/event.entity';

@Module({
  imports: [
  TypeOrmModule.forFeature([
      User,
      Region,
      Area,
      Center,
      Campaign,
      Item,
      Sertification,
      Image,
      Request,
      Notification,
      Event
    ]),
    AuthModule,
  ],
  providers: [UsersService, AdminService],
  exports: [UsersService, AdminService],
  controllers: [UsersController],
})
export class UsersModule {}
