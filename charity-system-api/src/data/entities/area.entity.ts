import { Region } from './region.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Center } from './center.entity';

@Entity({
  name: 'areas',
})
export class Area {
  @PrimaryGeneratedColumn('uuid')
  areaId: string;

  @Column()
  areaName: string;

  @ManyToOne(type => Region, region => region.areas)
  region: Promise<Region>;

  @OneToMany(type => Center, center => center.centerArea, { eager: true })
  areaCenters: Promise<Center[]>;
}
