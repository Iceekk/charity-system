"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const area_entity_1 = require("./area.entity");
const company_entity_1 = require("./company.entity");
const completionState_enum_1 = require("./../../models/enums/completionState.enum");
const heating_enum_1 = require("./../../models/enums/heating.enum");
const searchType_enum_1 = require("./../../models/enums/searchType.enum");
const construction_enum_1 = require("./../../models/enums/construction.enum");
const currencyType_enum_1 = require("./../../models/enums/currencyType.enum");
const estateType_enum_1 = require("./../../models/enums/estateType.enum");
const image_entity_1 = require("./image.entity");
const residenceState_enum_1 = require("./../../models/enums/residenceState.enum");
let Residence = class Residence {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Residence.prototype, "residenceId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Residence.prototype, "residenceName", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Residence.prototype, "residenceDescription", void 0);
__decorate([
    typeorm_1.Column({ enum: [searchType_enum_1.SearchType.SALES, searchType_enum_1.SearchType.RENTS], type: 'enum' }),
    __metadata("design:type", String)
], Residence.prototype, "residenceType", void 0);
__decorate([
    typeorm_1.Column({ default: '' }),
    __metadata("design:type", String)
], Residence.prototype, "residenceNeighborhood", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Residence.prototype, "residencePrice", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Residence.prototype, "residenceQuadrature", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], Residence.prototype, "residenceViews", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], Residence.prototype, "reportsCounter", void 0);
__decorate([
    typeorm_1.Column({ enum: [
            currencyType_enum_1.CurrencyType.BGN,
            currencyType_enum_1.CurrencyType.EUR
        ], type: 'enum' }),
    __metadata("design:type", String)
], Residence.prototype, "currencyType", void 0);
__decorate([
    typeorm_1.Column({ enum: [
            estateType_enum_1.EstateType.ONEBEDROOM,
            estateType_enum_1.EstateType.TWOBEDROOM,
            estateType_enum_1.EstateType.THREEBEDROOM,
            estateType_enum_1.EstateType.INDUSTRIAL_ROOM,
            estateType_enum_1.EstateType.MULTIROOM,
            estateType_enum_1.EstateType.MAISONETTE,
            estateType_enum_1.EstateType.OFFICE,
            estateType_enum_1.EstateType.HOUSE,
            estateType_enum_1.EstateType.FLOOR,
            estateType_enum_1.EstateType.VILLA,
            estateType_enum_1.EstateType.WAREHOUSE,
            estateType_enum_1.EstateType.AGRICULTURAL_PROPERTY,
            estateType_enum_1.EstateType.SHOP,
            estateType_enum_1.EstateType.HOTEL,
            estateType_enum_1.EstateType.GARAGE
        ], type: 'enum' }),
    __metadata("design:type", String)
], Residence.prototype, "residenceEstateType", void 0);
__decorate([
    typeorm_1.Column({ enum: [
            construction_enum_1.Construction.PANEL,
            construction_enum_1.Construction.BRICK,
            construction_enum_1.Construction.EPK,
            construction_enum_1.Construction.TEMPORARY_BUILDING,
            construction_enum_1.Construction.JOISTS,
            construction_enum_1.Construction.OTHER
        ], type: 'enum' }),
    __metadata("design:type", String)
], Residence.prototype, "residenceConstruction", void 0);
__decorate([
    typeorm_1.Column({ enum: [
            heating_enum_1.Heating.TEC,
            heating_enum_1.Heating.GAS,
            heating_enum_1.Heating.FIREPLACE,
            heating_enum_1.Heating.HEATPUMP,
            heating_enum_1.Heating.SOLARPANEL,
            heating_enum_1.Heating.ELECTRICITY,
            heating_enum_1.Heating.OTHER
        ], type: 'enum' }),
    __metadata("design:type", String)
], Residence.prototype, "residenceHeating", void 0);
__decorate([
    typeorm_1.Column({ enum: [
            completionState_enum_1.CompletionState.INPROJECT,
            completionState_enum_1.CompletionState.LUXURY,
            completionState_enum_1.CompletionState.WITH_FINISHING_WORKS,
            completionState_enum_1.CompletionState.WITHOUT_FINISHING_WORKS,
            completionState_enum_1.CompletionState.ACT15,
            completionState_enum_1.CompletionState.ACT16
        ], type: 'enum' }),
    __metadata("design:type", String)
], Residence.prototype, "residenceStageOfConstruction", void 0);
__decorate([
    typeorm_1.Column({ enum: [
            residenceState_enum_1.ResidenceState.FURNISHED,
            residenceState_enum_1.ResidenceState.UNFURNISHED
        ], type: 'enum' }),
    __metadata("design:type", String)
], Residence.prototype, "residenceState", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Residence.prototype, "residenceFeatures", void 0);
__decorate([
    typeorm_1.Column({ default: true }),
    __metadata("design:type", Boolean)
], Residence.prototype, "isActive", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", Date)
], Residence.prototype, "dateOfCreation", void 0);
__decorate([
    typeorm_1.ManyToOne(type => area_entity_1.Area, area => area.areaResidences),
    __metadata("design:type", Promise)
], Residence.prototype, "area", void 0);
__decorate([
    typeorm_1.ManyToOne(type => company_entity_1.Company, company => company.companyResidences),
    __metadata("design:type", Promise)
], Residence.prototype, "company", void 0);
__decorate([
    typeorm_1.OneToMany(type => image_entity_1.Image, image => image.residenceImage, { eager: true }),
    __metadata("design:type", Array)
], Residence.prototype, "residenceImages", void 0);
Residence = __decorate([
    typeorm_1.Entity({
        name: 'residences',
    })
], Residence);
exports.Residence = Residence;
//# sourceMappingURL=residence.entity.js.map