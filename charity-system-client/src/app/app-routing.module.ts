import { ReviewrequestsComponent } from './shared/components/reviewrequests/reviewrequests.component';
import { ViewrequestComponent } from './shared/components/viewrequest/viewrequest.component';
import { CalendarComponent } from './shared/components/calendar/calendar.component';
import { StatisticsComponent } from './shared/components/statistics/statistics.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuard } from './shared/authorization/role-guard.service';
import { AdminGuard } from './shared/authorization/admin-guard.service';
import { UnauthorisedComponent } from './shared/errors/unauthorised/unauthorised.component';
import { NotFoundComponent } from './shared/errors/not-found/not-found.component';
import { HomeComponent } from './shared/components/home/home.component';
import { AboutUsComponent } from './shared/components/about-us/about-us.component';
import { ProfileComponent } from './shared/components/profile/profile.component';
import { AwardsComponent } from './shared/components/awards/awards.component';
import { CentersListComponent } from './shared/components/centers-list/centers-list.component';
import { RankingComponent } from './shared/components/ranking/ranking.component';
import { CampaignsListComponent } from './shared/components/campaigns-list/campaigns-list.component';
import { CampaignComponent } from './shared/components/campaign/campaign.component';
import { UsersListComponent } from './shared/components/users-list/users-list.component';
import { AddItemComponent } from './admin/add-item/add-item.component';
import { MyrequestsComponent } from './shared/components/myrequests/myrequests.component';
import { NotificationsComponent } from './shared/components/notifications/notifications.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'campaigns',
    component: CampaignsListComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'users',
    component: UsersListComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'campaigns/:campaignId',
    component: CampaignComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'awards',
    component: AwardsComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'centers/:centerId',
    component: CentersListComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'calendar/:regionId',
    component: CalendarComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'ranking',
    component: RankingComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'statistics',
    component: StatisticsComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'my-requests',
    component: MyrequestsComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'review-requests',
    component: ReviewrequestsComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'notifications',
    component: NotificationsComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'requests/:requestId',
    component: ViewrequestComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'make-request',
    component: AddItemComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'profile/:userId',
    component: ProfileComponent,
    canActivate: [RoleGuard],
    data: { animation: { value: '' } }
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule',
    canActivate: [AdminGuard],
    data: { animation: { value: 'admin' } }
  },
  {
    path: 'unauthorised',
    component: UnauthorisedComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
