import { Area } from './area.entity';
import { Company } from './company.entity';
import { Image } from './image.entity';
export declare class Residence {
    residenceId: string;
    residenceName: string;
    residenceDescription: string;
    residenceType: string;
    residenceNeighborhood: string;
    residencePrice: number;
    residenceQuadrature: number;
    residenceViews: number;
    reportsCounter: number;
    currencyType: string;
    residenceEstateType: string;
    residenceConstruction: string;
    residenceHeating: string;
    residenceStageOfConstruction: string;
    residenceState: string;
    residenceFeatures: string;
    isActive: boolean;
    dateOfCreation: Date;
    area: Promise<Area>;
    company: Promise<Company>;
    residenceImages: Image[];
}
