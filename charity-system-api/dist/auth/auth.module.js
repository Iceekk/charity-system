"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_strategy_1 = require("./strategy/jwt.strategy");
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./auth.service");
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
const config_module_1 = require("./../config/config.module");
const config_service_1 = require("./../config/config.service");
const auth_controller_1 = require("./auth.controller");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../data/entities/user.entity");
const company_entity_1 = require("../data/entities/company.entity");
const region_entity_1 = require("../data/entities/region.entity");
const area_entity_1 = require("../data/entities/area.entity");
const notification_entity_1 = require("../data/entities/notification.entity");
const image_entity_1 = require("../data/entities/image.entity");
const residence_entity_1 = require("../data/entities/residence.entity");
const land_entity_1 = require("../data/entities/land.entity");
const statistic_entity_1 = require("../data/entities/statistic.entity");
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    common_1.Module({
        imports: [
            config_module_1.ConfigModule,
            passport_1.PassportModule.register({ defaultStrategy: 'jwt' }),
            jwt_1.JwtModule.registerAsync({
                imports: [config_module_1.ConfigModule],
                useFactory: async (configService) => ({
                    secretOrPrivateKey: configService.jwtSecret,
                    signOptions: {
                        expiresIn: configService.jwtExpireTime,
                    },
                }),
                inject: [config_service_1.ConfigService],
            }),
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.User, company_entity_1.Company, region_entity_1.Region, area_entity_1.Area, notification_entity_1.Notification, image_entity_1.Image, residence_entity_1.Residence, land_entity_1.Land, statistic_entity_1.Statistic]),
        ],
        providers: [auth_service_1.AuthService, jwt_strategy_1.JwtStrategy],
        exports: [auth_service_1.AuthService],
        controllers: [auth_controller_1.AuthController],
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map