import { AppConfig } from './../../config/app.config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserLogin } from 'src/app/shared/models/user/user-login.model';
import * as jwt_decode from 'jwt-decode';

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private appConfig: AppConfig, private http: HttpClient) {}

  public login(user: UserLogin): Observable<object> {
    return this.http.post(`${this.appConfig.apiUrl}/login`, user);
  }

  public getToken(): string {
    const token = localStorage.getItem('token');

    if (token) {
      return token;
    }
    return null;
  }

  public getRole(): string {
    const token = localStorage.getItem('token');

    if (token) {
      const decodedToken: any = jwt_decode(token);
      return decodedToken.role;
    }

    return null;
  }

  public getEmail(): string {
    const token = localStorage.getItem('token');

    if (token) {
      const decodedToken: any = jwt_decode(token);
      return decodedToken.email;
    }

    return null;
  }

  public getNumberOfSales() {
    return this.http.get(`${this.appConfig.apiUrl}/get-number-of-sales`);
  }

  public getNumberOfRents() {
    return this.http.get(`${this.appConfig.apiUrl}/get-number-of-rents`);
  }

  public getUnfilteredResidences(data) {
    return this.http.post(
      `${this.appConfig.apiUrl}/get-unfiltered-residences`,
      data
    );
  }

  public getUnfilteredLands(data) {
    return this.http.post(
      `${this.appConfig.apiUrl}/get-unfiltered-lands`,
      data
    );
  }

  public getSpecificEstate(data) {
    return this.http.post(`${this.appConfig.apiUrl}/get-specific-estate`, data);
  }

  public getFilteredEstates(data) {
    return this.http.post(
      `${this.appConfig.apiUrl}/get-filtered-estates`,
      data
    );
  }

  public registerUser(data: any) {
    const body = {
      email: data.emailReg,
      password: data.passwordReg,
      isOkayWithTermsAndConditions: true
    };

    return this.http.post(`${this.appConfig.apiUrl}/register/user`, body);
  }

  public logout(data: any) {
    const body = {
      userEmail: data
    };

    return this.http.post(`${this.appConfig.apiUrl}/logOut`, body);
  }

  //==========================================

  public getAllUsers() {
    return this.http.get(`${this.appConfig.apiUrl}/get-users`);
  }

  public getSpecificUser(data) {
    return this.http.post(`${this.appConfig.apiUrl}/get-user`,data);
  }

  public getAllCampaigns(): any {
    return this.http.get(`${this.appConfig.apiUrl}/get-campaigns`);
  }

  public getCampaignByStatus() {
    return this.http.get(`${this.appConfig.apiUrl}/get-campaign-by-status`);
  }

  public getAllPendingRequests() {
    return this.http.get(`${this.appConfig.apiUrl}/get-all-pending-requests`);
  }

  public hasRequestsForReview() {
    return this.http.get(`${this.appConfig.apiUrl}/has-requests-for-review`);
  }

  public getPieData() {
    return this.http.get(`${this.appConfig.apiUrl}/get-pie-data`);
  }

  public getXYData() {
    return this.http.get(`${this.appConfig.apiUrl}/get-xy-data`);
  }

  public getAllTransactions() {
    return this.http.get(`${this.appConfig.apiUrl}/get-all-transactions`);
  }

  public addSertificate(data) {
    return this.http.post(`${this.appConfig.apiUrl}/add-sertificate`, data);
  }

  public hasMessagesForReview(data) {
    return this.http.post(`${this.appConfig.apiUrl}/has-messages-for-review`, data);
  }

  public getSpecificCampaign(data) {
    return this.http.post(`${this.appConfig.apiUrl}/get-specific-campaign`, data);
  }

  public getUserNotifications(data) {
    return this.http.post(`${this.appConfig.apiUrl}/get-user-notifications`, data);
  }

  public getRequestByNumber(data) {
    return this.http.post(`${this.appConfig.apiUrl}/get-request-by-number`, data);
  }

  public createEvent(data) {
    return this.http.post(`${this.appConfig.apiUrl}/create-event`, data);
  }

  public updateEvents(data) {
    return this.http.post(`${this.appConfig.apiUrl}/update-events`, data);
  }

  public getUserEvents(data) {
    return this.http.post(`${this.appConfig.apiUrl}/get-user-events`, data);
  }

  public deleteEvent(data) {
    return this.http.post(`${this.appConfig.apiUrl}/delete-event`, data);
  }

  public deleteNotification(data) {
    return this.http.post(`${this.appConfig.apiUrl}/delete-notification`, data);
  }

  public editSpecificCampaign(data) {
    return this.http.post(`${this.appConfig.apiUrl}/edit-specific-campaign`, data);
  }

  public getAllCenters(data): any {
    return this.http.post(`${this.appConfig.apiUrl}/get-centers`, data);
  }

  public getSpecificCenter(data): any {
    return this.http.post(`${this.appConfig.apiUrl}/get-specific-center`, data);
  }

  public deleteCenter(data) {
    return this.http.post(`${this.appConfig.apiUrl}/delete-center`, data);
  }

  public editCenter(data) {
    return this.http.post(`${this.appConfig.apiUrl}/edit-center`, data);
  }

  public makeRequest(data) {
    return this.http.post(`${this.appConfig.apiUrl}/make-request`, data);
  }

  public getUserRequests(data) {
    return this.http.post(`${this.appConfig.apiUrl}/get-user-requests`, data);
  }

  public getSpecificRequest(data) {
    return this.http.post(`${this.appConfig.apiUrl}/get-specific-request`, data);
  }

  public resolveRequest(data) {
    return this.http.post(`${this.appConfig.apiUrl}/resolve-request`, data);
  }
}
