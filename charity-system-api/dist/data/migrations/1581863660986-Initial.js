"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Initial1581863660986 {
    async up(queryRunner) {
        await queryRunner.query("CREATE TABLE `regions` (`regionId` varchar(255) NOT NULL, `regionName` varchar(255) NOT NULL, PRIMARY KEY (`regionId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `images` (`id` varchar(255) NOT NULL, `fileName` varchar(255) NOT NULL, `filePath` varchar(255) NOT NULL, `creationDate` datetime NOT NULL, `residenceImageResidenceId` varchar(255) NULL, `landImageLandId` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `lands` (`landId` varchar(255) NOT NULL, `landName` varchar(255) NOT NULL, `landDescription` varchar(255) NOT NULL DEFAULT '', `landType` enum ('продажби', 'наеми') NOT NULL, `landNeighborhood` varchar(255) NOT NULL DEFAULT '', `landPrice` int NOT NULL, `landQuadrature` int NOT NULL, `landViews` int NOT NULL DEFAULT 0, `reportsCounter` int NOT NULL DEFAULT 0, `currencyType` enum ('BGN', 'EUR') NOT NULL, `landEstateType` enum ('едностаен', 'двустаен', 'тристаен', 'промишлено помещение', 'многостаен', 'мезонет', 'офис', 'къща', 'етаж', 'вила', 'склад', 'земеделски имот', 'търговски обект', 'хотел', 'гараж') NOT NULL, `landFeatures` varchar(255) NULL, `isActive` tinyint NOT NULL DEFAULT 1, `dateOfCreation` datetime NULL, `areaAreaId` varchar(255) NULL, `companyCompanyId` varchar(255) NULL, PRIMARY KEY (`landId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `companies` (`companyId` varchar(255) NOT NULL, `companyName` varchar(255) NOT NULL, `companyPhone` varchar(255) NOT NULL, `companyEmail` varchar(255) NOT NULL, `companySiteURL` varchar(255) NOT NULL, `companyAddress` varchar(255) NOT NULL, UNIQUE INDEX `IDX_89c6967fd03128e11e33e1bd77` (`companyEmail`), PRIMARY KEY (`companyId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `residences` (`residenceId` varchar(255) NOT NULL, `residenceName` varchar(255) NOT NULL, `residenceDescription` varchar(255) NOT NULL, `residenceType` enum ('продажби', 'наеми') NOT NULL, `residenceNeighborhood` varchar(255) NOT NULL DEFAULT '', `residencePrice` int NOT NULL, `residenceQuadrature` int NOT NULL, `residenceViews` int NOT NULL DEFAULT 0, `reportsCounter` int NOT NULL DEFAULT 0, `currencyType` enum ('BGN', 'EUR') NOT NULL, `residenceEstateType` enum ('едностаен', 'двустаен', 'тристаен', 'промишлено помещение', 'многостаен', 'мезонет', 'офис', 'къща', 'етаж', 'вила', 'склад', 'земеделски имот', 'търговски обект', 'хотел', 'гараж') NOT NULL, `residenceConstruction` enum ('панел', 'тухла', 'ЕПК', 'временна постройка', 'гредоред', 'друго') NOT NULL, `residenceHeating` enum ('ТЕЦ', 'газ', 'камина', 'термопомпа', 'соларен панел', 'елетричество', 'друго') NOT NULL, `residenceStageOfConstruction` enum ('в проект', 'лукс', 'с довършителни работи', 'без довършителни работи', 'акт 15', 'акт 16') NOT NULL, `residenceState` enum ('обзаведен', 'необзаведен') NOT NULL, `residenceFeatures` varchar(255) NULL, `isActive` tinyint NOT NULL DEFAULT 1, `dateOfCreation` datetime NULL, `areaAreaId` varchar(255) NULL, `companyCompanyId` varchar(255) NULL, PRIMARY KEY (`residenceId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `areas` (`areaId` varchar(255) NOT NULL, `areaName` varchar(255) NOT NULL, `regionRegionId` varchar(255) NULL, PRIMARY KEY (`areaId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `notifications` (`notificationId` varchar(255) NOT NULL, `reportedEstateId` varchar(255) NOT NULL, `reportedByEmail` varchar(255) NOT NULL, PRIMARY KEY (`notificationId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `statistics` (`statisticId` varchar(255) NOT NULL, `statisticDate` datetime NOT NULL, `numberOfVisitors` int NOT NULL DEFAULT 0, `numberOfRegisteredVisitors` int NOT NULL DEFAULT 0, `createdEstates` int NOT NULL DEFAULT 0, `deletedEstates` int NOT NULL DEFAULT 0, UNIQUE INDEX `IDX_25fe5f4872da0d2260269007f4` (`statisticDate`), PRIMARY KEY (`statisticId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`userId` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `role` enum ('ADMIN', 'USER', 'PAID_USER_3', 'PAID_USER_6', 'PAID_USER_12') NOT NULL DEFAULT 'USER', `dateOfPayment` datetime NULL, `isLogged` tinyint NOT NULL DEFAULT 0, `isOkayWithTermsAndConditions` tinyint NOT NULL DEFAULT 0, `typeOfPayment` varchar(255) NOT NULL DEFAULT 'NOT_PAID', `favouriteEstates` varchar(255) NOT NULL DEFAULT '', UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`userId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `images` ADD CONSTRAINT `FK_3c5aac3f515af69516cecef6b9d` FOREIGN KEY (`residenceImageResidenceId`) REFERENCES `residences`(`residenceId`)");
        await queryRunner.query("ALTER TABLE `images` ADD CONSTRAINT `FK_c04591eeeef6f10c9cdd7d64f2b` FOREIGN KEY (`landImageLandId`) REFERENCES `lands`(`landId`)");
        await queryRunner.query("ALTER TABLE `lands` ADD CONSTRAINT `FK_1fce5fa0adf9cca62a4471b7edb` FOREIGN KEY (`areaAreaId`) REFERENCES `areas`(`areaId`)");
        await queryRunner.query("ALTER TABLE `lands` ADD CONSTRAINT `FK_4c394fd26450d7fe1404202bd0d` FOREIGN KEY (`companyCompanyId`) REFERENCES `companies`(`companyId`)");
        await queryRunner.query("ALTER TABLE `residences` ADD CONSTRAINT `FK_616f1dd9cb92cc0df76009f2e3c` FOREIGN KEY (`areaAreaId`) REFERENCES `areas`(`areaId`)");
        await queryRunner.query("ALTER TABLE `residences` ADD CONSTRAINT `FK_562314d3fccb7b70f7cb9c18610` FOREIGN KEY (`companyCompanyId`) REFERENCES `companies`(`companyId`)");
        await queryRunner.query("ALTER TABLE `areas` ADD CONSTRAINT `FK_d3d55ca6dfa50d857d76b7a20e2` FOREIGN KEY (`regionRegionId`) REFERENCES `regions`(`regionId`)");
    }
    async down(queryRunner) {
        await queryRunner.query("ALTER TABLE `areas` DROP FOREIGN KEY `FK_d3d55ca6dfa50d857d76b7a20e2`");
        await queryRunner.query("ALTER TABLE `residences` DROP FOREIGN KEY `FK_562314d3fccb7b70f7cb9c18610`");
        await queryRunner.query("ALTER TABLE `residences` DROP FOREIGN KEY `FK_616f1dd9cb92cc0df76009f2e3c`");
        await queryRunner.query("ALTER TABLE `lands` DROP FOREIGN KEY `FK_4c394fd26450d7fe1404202bd0d`");
        await queryRunner.query("ALTER TABLE `lands` DROP FOREIGN KEY `FK_1fce5fa0adf9cca62a4471b7edb`");
        await queryRunner.query("ALTER TABLE `images` DROP FOREIGN KEY `FK_c04591eeeef6f10c9cdd7d64f2b`");
        await queryRunner.query("ALTER TABLE `images` DROP FOREIGN KEY `FK_3c5aac3f515af69516cecef6b9d`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP INDEX `IDX_25fe5f4872da0d2260269007f4` ON `statistics`");
        await queryRunner.query("DROP TABLE `statistics`");
        await queryRunner.query("DROP TABLE `notifications`");
        await queryRunner.query("DROP TABLE `areas`");
        await queryRunner.query("DROP TABLE `residences`");
        await queryRunner.query("DROP INDEX `IDX_89c6967fd03128e11e33e1bd77` ON `companies`");
        await queryRunner.query("DROP TABLE `companies`");
        await queryRunner.query("DROP TABLE `lands`");
        await queryRunner.query("DROP TABLE `images`");
        await queryRunner.query("DROP TABLE `regions`");
    }
}
exports.Initial1581863660986 = Initial1581863660986;
//# sourceMappingURL=1581863660986-Initial.js.map