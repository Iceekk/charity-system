import { JwtPayload } from '../../models/interfaces/jwt-payload';
import { ConfigService } from './../../config/config.service';
import { AuthService } from './../auth.service';
import { GetUserDTO } from '../../models/users/get-user.dto';
declare const JwtStrategy_base: new (...args: any[]) => any;
export declare class JwtStrategy extends JwtStrategy_base {
    private readonly authService;
    private readonly configService;
    constructor(authService: AuthService, configService: ConfigService);
    validate(payload: JwtPayload): Promise<GetUserDTO>;
}
export {};
