"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const common_1 = require("@nestjs/common");
class CreateResidenceDTO {
}
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "name", void 0);
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "description", void 0);
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "type", void 0);
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "neighborhood", void 0);
__decorate([
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CreateResidenceDTO.prototype, "price", void 0);
__decorate([
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CreateResidenceDTO.prototype, "quadrature", void 0);
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "typeOfProperty", void 0);
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "construction", void 0);
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "state", void 0);
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "heating", void 0);
__decorate([
    class_validator_1.IsNumber(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "floor", void 0);
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "stageOfConstruction", void 0);
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", Boolean)
], CreateResidenceDTO.prototype, "isActive", void 0);
__decorate([
    common_1.Optional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreateResidenceDTO.prototype, "images", void 0);
exports.CreateResidenceDTO = CreateResidenceDTO;
//# sourceMappingURL=create-offer.dto.js.map