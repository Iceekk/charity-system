import { Directive, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({selector: '[appClickCatcher]'})
export class ClickCatcherDirective {

    currentAreaId = 'BG-01';

    @Output() onUpdateProperty: EventEmitter<any> = new EventEmitter();


    @HostListener('click', ['$event.target.id']) onClick(id: any) {

        this.onUpdateProperty.emit(id);

        if (id !== null && id !== undefined && id.length !== 0) {

            if (this.currentAreaId.toString().indexOf('point') !== -1) {
              document.getElementById(this.currentAreaId).style.fill = 'white';
            } else {
              document.getElementById(this.currentAreaId).style.fill = '#1495ba';
            }

            this.currentAreaId = id;
            document.getElementById(id).style.fill = '#ba14a4';
        } else {
            if (this.currentAreaId.toString().indexOf('point') !== -1) {
              console.log(id);
              document.getElementById(this.currentAreaId).style.fill = 'white';
            } else {
              document.getElementById(this.currentAreaId).style.fill = '#1495ba';
            }
        }
    }

    update(event: any) {
        // document.getElementById('mytext').value = '';
        // this.updateProperty.emit(this.el.nativeElement.innerText);
    }

}
