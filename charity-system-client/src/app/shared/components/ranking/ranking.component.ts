import { Component, OnInit } from '@angular/core';
import { ElementRef, HostListener, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MdbTableDirective, MdbTablePaginationComponent } from 'angular-bootstrap-md';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit, AfterViewInit  {

  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination1: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable1: MdbTableDirective
  elements1: any = [];
  previous1: any = [];
  headElements1 = ['Place', 'Name', 'Email', 'Points'];
  // ==============================================================================================================

  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild('row', { static: true }) row: ElementRef;

  elements: any = [];
  headElements = ['Place', 'Name', 'Email', 'Items number'];

  searchText: string = '';
  previous: string;

  maxVisibleItems: number = 8;

  constructor(
    private cdRef: ChangeDetectorRef, 
    private cdRef1: ChangeDetectorRef,
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) {}

  @HostListener('input') oninput() {
    this.mdbTablePagination.searchText = this.searchText;
  }

  ngOnInit() {

    this.userService.getAllUsers().subscribe((data: any) => {

      const users = data.sort((a, b) => {
        return b.totalItemsDonated - a.totalItemsDonated;
      });
      
      users.forEach((el, index) => {
        this.elements.push({place: index + 1, name: el.firstName + ' ' + el.lastName, email: el.email, points: el.totalItemsDonated});
      });

      this.mdbTable.setDataSource(this.elements);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();
    }, (err) => {
    });

    this.userService.getAllUsers().subscribe((data: any) => {

      const users = data.sort((a, b) => {
        return b.points - a.points;
      });
      
      users.forEach((el, index) => {
        this.elements1.push({place: index + 1, name: el.firstName + ' ' + el.lastName, email: el.email, points: el.points});
      });

      this.mdbTable1.setDataSource(this.elements1);
      this.elements1 = this.mdbTable1.getDataSource();
      this.previous1 = this.mdbTable1.getDataSource();
    }, (err) => {
    });
  }

  ngAfterViewInit() {
    // otggore e dqsnata klasaciq
    this.mdbTablePagination1.setMaxVisibleItemsNumberTo(9);

    this.mdbTablePagination1.calculateFirstItemIndex();
    this.mdbTablePagination1.calculateLastItemIndex();
    this.cdRef1.detectChanges();
    //=======================================================================================
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(9);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  addNewRow() {
    this.mdbTable.addRow({
      place: this.elements.length.toString(),
      name: 'Wpis ' + this.elements.length,
      email: 'Last ' + this.elements.length,
      points: 'Handle ' + this.elements.length
    });
    this.emitDataSourceChange();
  }

  addNewRowAfter() {
    this.mdbTable.addRowAfter(1, {place: 3, name: 'Nowy', email: 'Row', points: 2});
    this.mdbTable.getDataSource().forEach((el: any, index: any) => {
      el.points = (index + 1).toString();
    });
    this.emitDataSourceChange();
  }

  removeLastRow() {
    this.mdbTable.removeLastRow();
    this.emitDataSourceChange();
    this.mdbTable.rowRemoved().subscribe((data: any) => {
    });
  }

  removeRow() {
    this.mdbTable.removeRow(1);
    this.mdbTable.getDataSource().forEach((el: any, index: any) => {
      el.id = (index + 1).toString();
    });
    this.emitDataSourceChange();
    this.mdbTable.rowRemoved().subscribe((data: any) => {
      console.log(data);
    });
  }

  emitDataSourceChange() {
    this.mdbTable.dataSourceChange().subscribe((data: any) => {
      console.log(data);
    });
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

}
