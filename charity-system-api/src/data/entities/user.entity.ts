import { Notification } from './notification.entity';
import { Role } from './../../models/enums/roles.enum';
import { Column, PrimaryGeneratedColumn, Entity, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Sertification } from './sertification.entity';
import { Campaign } from './campaign.entity';
import { Item } from './item.entity';
import { Request } from './request.entity';

@Entity({
  name: 'users',
})
export class User {
  @PrimaryGeneratedColumn('uuid')
  userId: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({
    enum: [
      Role.ADMIN,
      Role.USER,
      Role.PAID_USER_3,
      Role.PAID_USER_6,
      Role.PAID_USER_12,
    ],
    type: 'enum',
    default: Role.USER,
  })
  role: string;

  @Column({ default: 0 })
  points: number;

  @Column({ default: 0 })
  totalItemsDonated: number;

  @Column({ default: 0 })
  totalMoneyDonated: number;

  @Column({ default: false })
  isLogged: boolean;

  @Column({ default: false })
  isOkayWithTermsAndConditions: boolean;

  @OneToMany((type) => Sertification, sertification => sertification.sertificationImage)
  userSertifications: Sertification[];

  @OneToMany((type) => Notification, notification => notification.userNotify)
  userNotifications: Notification[];

  @OneToMany((type) => Request, request => request.requestAuthor)
  userRequests: Request[];

  // @ManyToMany(() => Item)
  // @JoinTable()
  // currDonatedItems: Item[];
}
