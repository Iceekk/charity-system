"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_1 = require("@nestjs/jwt");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../data/entities/user.entity");
const typeorm_2 = require("typeorm");
const bcrypt = require("bcrypt");
const roles_enum_1 = require("../models/enums/roles.enum");
let AuthService = class AuthService {
    constructor(usersRepository, jwtService) {
        this.usersRepository = usersRepository;
        this.jwtService = jwtService;
    }
    async signIn(user) {
        const userFound = await this.usersRepository
            .findOne({ select: ['email', 'password', 'role'], where: { email: user.email } });
        if (userFound) {
            const result = await bcrypt.compare(user.password, userFound.password);
            if (result) {
                return this
                    .jwtService
                    .sign({ email: userFound.email, role: userFound.role });
            }
        }
        return null;
    }
    async validateUser(payload) {
        return null;
    }
    async registerUser(user) {
        const userFound = await this.usersRepository.findOne({ where: { email: user.email } });
        if (userFound) {
            throw new common_1.HttpException('User already exists!', common_1.HttpStatus.FORBIDDEN);
        }
        user.password = await bcrypt.hash(user.password, 10);
        user.role = roles_enum_1.Role.ADMIN;
        const createUser = await this.usersRepository.create(user);
        const result = await this.usersRepository.save(createUser);
        return result;
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map