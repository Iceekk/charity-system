import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewrequestsComponent } from './reviewrequests.component';

describe('ReviewrequestsComponent', () => {
  let component: ReviewrequestsComponent;
  let fixture: ComponentFixture<ReviewrequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewrequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewrequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
