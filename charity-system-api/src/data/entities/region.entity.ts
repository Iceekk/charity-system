import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Area } from './area.entity';
import { Event } from './event.entity';

@Entity({
  name: 'regions',
})
export class Region {
  @PrimaryGeneratedColumn('uuid')
  regionId: string;

  @Column()
  regionName: string;

  @OneToMany(type => Area, area => area.region, { eager: true })
  areas: Area[];

  @OneToMany(type => Event, event => event.eventRegion, { eager: true })
  events: Event[];
}
