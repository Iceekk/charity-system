"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BasicStatus;
(function (BasicStatus) {
    BasicStatus["ACTIVE"] = "ACTIVE";
    BasicStatus["ARCHIVED"] = "ARCHIVED";
})(BasicStatus = exports.BasicStatus || (exports.BasicStatus = {}));
//# sourceMappingURL=basicstatus.enum.js.map