import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TokenInterceptor } from './shared/interceptors/token.interceptor';
import { SpinnerInterceptor } from './shared/interceptors/spinner.interceptor';
import { AdminGuard } from './shared/authorization/admin-guard.service';
import { UserGuard } from './shared/authorization/user-guard.service';
import { RoleGuard } from './shared/authorization/role-guard.service';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AdminModule } from './admin/admin.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgxSpinnerModule,
    SharedModule,
    MDBBootstrapModule.forRoot(),
    AdminModule,
    ToastrModule.forRoot(),
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    AdminGuard,
    UserGuard,
    RoleGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
