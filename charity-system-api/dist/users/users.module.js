"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const auth_module_1 = require("./../auth/auth.module");
const typeorm_1 = require("@nestjs/typeorm");
const common_1 = require("@nestjs/common");
const user_entity_1 = require("./../data/entities/user.entity");
const users_controller_1 = require("./users.controller");
const region_entity_1 = require("../data/entities/region.entity");
const residence_entity_1 = require("../data/entities/residence.entity");
const area_entity_1 = require("../data/entities/area.entity");
const company_entity_1 = require("../data/entities/company.entity");
const users_service_1 = require("./users.service");
const estate_service_1 = require("./estate.service");
const land_entity_1 = require("../data/entities/land.entity");
const statistic_entity_1 = require("../data/entities/statistic.entity");
const notification_entity_1 = require("../data/entities/notification.entity");
const image_entity_1 = require("../data/entities/image.entity");
let UsersModule = class UsersModule {
};
UsersModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([user_entity_1.User, region_entity_1.Region, area_entity_1.Area, company_entity_1.Company, residence_entity_1.Residence, land_entity_1.Land, statistic_entity_1.Statistic, notification_entity_1.Notification, image_entity_1.Image]), auth_module_1.AuthModule],
        providers: [users_service_1.UsersService, estate_service_1.EstateService],
        exports: [users_service_1.UsersService, estate_service_1.EstateService],
        controllers: [users_controller_1.UsersController],
    })
], UsersModule);
exports.UsersModule = UsersModule;
//# sourceMappingURL=users.module.js.map