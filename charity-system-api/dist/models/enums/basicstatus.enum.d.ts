export declare enum BasicStatus {
    ACTIVE = "ACTIVE",
    ARCHIVED = "ARCHIVED"
}
