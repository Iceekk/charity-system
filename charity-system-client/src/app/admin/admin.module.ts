import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-route.module';
import { SharedModule } from '../shared/shared.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DragDropComponent } from './drag-drop/drag-drop.component';
import { AddCampaignComponent } from '../admin/add-campaign/add-campaign.component';
import { AddCenterComponent } from '../admin/add-center/add-center.component';
import { AddItemComponent } from '../admin/add-item/add-item.component';
import { EditCampaignComponent } from './edit-campaign/edit-campaign.component';
import { EditCenterComponent } from './edit-center/edit-center.component';

@NgModule({
  declarations: [
    AdminComponent,
    DragDropComponent,
    AddCampaignComponent,
    AddCenterComponent,
    AddItemComponent,
    EditCampaignComponent,
    EditCenterComponent
  ],
  imports: [
    SharedModule,
    AdminRoutingModule,
    MDBBootstrapModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot()
  ],
  entryComponents: []
})
export class AdminModule {}
