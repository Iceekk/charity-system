import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/admin/admin.service';
import { AppConfig } from 'src/app/config/app.config';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css']
})
export class CampaignComponent implements OnInit {

  campaign: any = {};
  items: any = {}

  user = {
    isAdmin: false
  };
  
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private adminService: AdminService,
    private toastService: ToastrService,
    private router: Router,
    private appConfig: AppConfig,
  ) { }

  ngOnInit() {

    this.user.isAdmin = this.userService.getRole() === 'ADMIN' ? true : false;

    this.galleryOptions = [
      {
          width: '100%',
          height: '500px',
          previewCloseOnEsc: true,
          previewKeyboardNavigation: true,
          imageBullets: true,
          imageAutoPlay: false,
          thumbnailsColumns: 4,
          imageAnimation: NgxGalleryAnimation.Slide
      },
      // max-width 800
      {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20
      },
      // max-width 400
      {
          breakpoint: 400,
          preview: false
      }
    ];
    
    this.galleryImages = [];

    this.userService.getSpecificCampaign({campaignId: this.route.snapshot.params.campaignId}).subscribe((data: any) => {

      this.items = data.tempItems;

      this.campaign = data;
      this.campaign.campaignStartDate = this.campaign.campaignStartDate.substring(0,10);
      this.campaign.campaignEndDate = this.campaign.campaignEndDate.substring(0,10);

      this.galleryImages = data.campaignImages.map((image: any) => {

        const img = (this.appConfig.apiUrl + (image.filePath.toString().substring(6).replace(/\\/g, '/'))).trim();

        return {small: img, medium: img, big: img};
      });
    }, (err) => {
      // this.toastService.error('', 'Вече има съществуващ потребител с въведените данни!');
    });
  }

}
