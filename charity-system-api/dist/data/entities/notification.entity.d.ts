export declare class Notification {
    notificationId: string;
    reportedEstateId: string;
    reportedByEmail: string;
}
