"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EstateType;
(function (EstateType) {
    EstateType["ONEBEDROOM"] = "\u0435\u0434\u043D\u043E\u0441\u0442\u0430\u0435\u043D";
    EstateType["TWOBEDROOM"] = "\u0434\u0432\u0443\u0441\u0442\u0430\u0435\u043D";
    EstateType["THREEBEDROOM"] = "\u0442\u0440\u0438\u0441\u0442\u0430\u0435\u043D";
    EstateType["INDUSTRIAL_ROOM"] = "\u043F\u0440\u043E\u043C\u0438\u0448\u043B\u0435\u043D\u043E \u043F\u043E\u043C\u0435\u0449\u0435\u043D\u0438\u0435";
    EstateType["MULTIROOM"] = "\u043C\u043D\u043E\u0433\u043E\u0441\u0442\u0430\u0435\u043D";
    EstateType["MAISONETTE"] = "\u043C\u0435\u0437\u043E\u043D\u0435\u0442";
    EstateType["OFFICE"] = "\u043E\u0444\u0438\u0441";
    EstateType["HOUSE"] = "\u043A\u044A\u0449\u0430";
    EstateType["FLOOR"] = "\u0435\u0442\u0430\u0436";
    EstateType["VILLA"] = "\u0432\u0438\u043B\u0430";
    EstateType["WAREHOUSE"] = "\u0441\u043A\u043B\u0430\u0434";
    EstateType["AGRICULTURAL_PROPERTY"] = "\u0437\u0435\u043C\u0435\u0434\u0435\u043B\u0441\u043A\u0438 \u0438\u043C\u043E\u0442";
    EstateType["SHOP"] = "\u0442\u044A\u0440\u0433\u043E\u0432\u0441\u043A\u0438 \u043E\u0431\u0435\u043A\u0442";
    EstateType["HOTEL"] = "\u0445\u043E\u0442\u0435\u043B";
    EstateType["GARAGE"] = "\u0433\u0430\u0440\u0430\u0436";
})(EstateType = exports.EstateType || (exports.EstateType = {}));
//# sourceMappingURL=estateType.enum.js.map