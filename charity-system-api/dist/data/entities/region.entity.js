"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const area_entity_1 = require("./area.entity");
let Region = class Region {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Region.prototype, "regionId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Region.prototype, "regionName", void 0);
__decorate([
    typeorm_1.OneToMany(type => area_entity_1.Area, area => area.region, { eager: true }),
    __metadata("design:type", Array)
], Region.prototype, "areas", void 0);
Region = __decorate([
    typeorm_1.Entity({
        name: 'regions',
    })
], Region);
exports.Region = Region;
//# sourceMappingURL=region.entity.js.map