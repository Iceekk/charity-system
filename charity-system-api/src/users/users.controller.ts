import { AuthGuard } from '@nestjs/passport';
import {
  Controller,
  Get,
  UseGuards,
  Post,
  Body,
  UseInterceptors,
  FilesInterceptor,
  UploadedFiles,
} from '@nestjs/common';
import { UsersService } from './users.service';
import * as path from 'path';
import { diskStorage } from 'multer';
import { AdminService } from './admin.service';
import { AdminGuard } from 'src/common/guards/admin.guard';

@Controller('')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly adminService: AdminService,
  ) {}

  @Post('add-campaign')
  @UseGuards(AuthGuard(), AdminGuard)
  addCampaign(@Body() body: any): Promise<object> {
    return this.usersService.addCampaign(body);
  }

  @Post('add-center')
  @UseGuards(AuthGuard(), AdminGuard)
  addCenter(@Body() body: any): Promise<object> {
    return this.usersService.addCenter(body);
  }

  @Post('add-item')
  @UseGuards(AuthGuard(), AdminGuard)
  addItem(@Body() body: any): Promise<object> {
    return this.usersService.addItem(body);
  }

  @Get('add-items')
  addItems(): Promise<string> {
    return this.usersService.addItems();
  }

  @Get('get-users')
  getAllUsers(): Promise<object> {
    return this.usersService.getAllUsers();
  }

  @Post('get-user')
  getUserData(@Body() body: any): Promise<object> {
    return this.usersService.getUserData(body);
  }

  @Post('add-sertificate')
  addSertificate(@Body() body: any): Promise<object> {
    return this.usersService.addSertificate(body);
  }

  @Get('get-campaigns')
  getCampaigns(): Promise<object> {
    return this.usersService.getAllCampaigns();
  }

  @Get('get-campaign-by-status')
  getCampaignByStatus(): Promise<object> {
    return this.usersService.getCampaignByStatus();
  }

  @Post('get-specific-campaign')
  getSpecificCampaign(@Body() body: any): Promise<object> {
    return this.usersService.getSpecificCampaign(body);
  }

  @Post('edit-specific-campaign')
  editSpecificCampaign(@Body() body: any): Promise<object> {
    return this.usersService.editSpecificCampaign(body);
  }

  @Post('get-centers')
  getAllCenters(@Body() body: any): Promise<object> {
    return this.usersService.getAllCenters(body);
  }

  @Post('get-specific-center')
  getSpecificCenter(@Body() body: any): Promise<object> {
    return this.usersService.getSpecificCenter(body);
  }

  @Post('delete-center')
  deleteCenter(@Body() body: any): Promise<object> {
    return this.usersService.deleteCenter(body);
  }

  @Post('edit-center')
  editCenter(@Body() body: any): Promise<object> {
    return this.usersService.editCenter(body);
  }

  @Post('make-request')
  makeRequest(@Body() body: any): Promise<object> {
    return this.usersService.makeRequest(body);
  }

  @Post('get-user-requests')
  getUserRequests(@Body() body: any): Promise<object> {
    return this.usersService.getUserRequests(body);
  }

  @Post('get-specific-request')
  getSpecificRequest(@Body() body: any): Promise<object> {
    return this.usersService.getSpecificRequest(body);
  }

  @Post('resolve-request')
  resolveRequest(@Body() body: any): Promise<object> {
    return this.usersService.resolveRequest(body);
  }

  @Get('get-all-pending-requests')
  getAllPendingRequests(): Promise<object> {
    return this.usersService.getAllPendingRequests();
  }

  @Get('has-requests-for-review')
  hasRequestsForReview(): Promise<object> {
    return this.usersService.hasRequestsForReview();
  }

  @Get('get-pie-data')
  getPieData(): Promise<object> {
    return this.usersService.getPieData();
  }

  @Get('get-xy-data')
  getXYData(): Promise<object> {
    return this.usersService.getXYData();
  }

  @Get('get-all-transactions')
  getAllTransactions(): Promise<object> {
    return this.usersService.getAllTransactions();
  }

  @Post('get-user-notifications')
  getUserNotifications(@Body() body: any): Promise<object> {
    return this.usersService.getUserNotifications(body);
  }

  @Post('create-event')
  createEvent(@Body() body: any): Promise<object> {
    return this.usersService.createEvent(body);
  }

  @Post('update-events')
  updateEvents(@Body() body: any): Promise<object> {
    return this.usersService.updateEvents(body);
  }

  @Post('delete-event')
  deleteEvent(@Body() body: any): Promise<object> {
    return this.usersService.deleteEvent(body);
  }

  @Post('get-user-events')
  getUserEvents(@Body() body: any): Promise<object> {
    return this.usersService.getUserEvents(body);
  }

  @Post('delete-notification')
  deleteNotification(@Body() body: any): Promise<object> {
    return this.usersService.deleteNotification(body);
  }

  @Post('get-request-by-number')
  getRequestByNumber(@Body() body: any): Promise<object> {
    return this.usersService.getRequestByNumber(body);
  }

  @Post('has-messages-for-review')
  hasMessagesForReview(@Body() body: any): Promise<object> {
    return this.usersService.hasMessagesForReview(body);
  }


  @Get('add-admin')
  addAdmin(): Promise<string> {
    return this.usersService.addAdmin();
  }

  @Post('uploadImages')
  @UseGuards(AuthGuard())
  @UseInterceptors(
    FilesInterceptor('estateImages', 999, {
      storage: diskStorage({
        destination: './public/estates',
        filename: (req, file, cb) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          return cb(null, `${randomName}${path.extname(file.originalname)}`);
        },
      }),
      limits: {
        fileSize: 33097152 //33 Megabytes
      },
    }),
  )
  async uploadImages(
    @UploadedFiles() images,
    @Body() fileDto: any,
  ): Promise<object> {
    return { images };
  }
  // https://github.com/fastify/fastify-multipart

  @Get('fill-regions')
  fillDatabaseWithData(): Promise<string> {
    return this.usersService.fillRegions();
  }

  @Get('test')
  getClientInfo(): any {
    // return this.usersService.createStatisticData();
    return 'heii';
  }
}
