import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-viewrequest',
  templateUrl: './viewrequest.component.html',
  styleUrls: ['./viewrequest.component.css']
})
export class ViewrequestComponent implements OnInit {

  isAdmin: any = false;
  headElements = ['ID', 'Category', 'Name', 'Quantity', 'Description', 'Points'];
  itemsArr: any = [];
  requestObj: any = {};
  public landForm: FormGroup;

  constructor(
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.isAdmin = this.userService.getRole() === 'ADMIN' ? true : false;

    this.userService.getSpecificRequest({reqId: this.route.snapshot.params.requestId}).subscribe(
      (dataa: any) => {

        this.requestObj = {
          reqNumber: dataa.requestNumber,
          reqStatus: dataa.requestStatus,
          reqDate: dataa.requestDate
        };

        dataa.__requestItems__.forEach(element => {
          
          this.itemsArr.push({
            id: this.itemsArr.length + 1,
            itemQuantity: element.itemQuantity,
            itemPoints: element.itemPoints,
            itemCat: element.itemCategory,
            itemName: element.itemName,
            itemDesc: element.itemDescription,
          });
        });
      },
      err => {
      }
    );

    this.buildTheForm();
  }

  public buildTheForm() {
    this.landForm = this.formBuilder.group({
      itemDescription: this.formBuilder.control('')
    });
  }

  confirmRequest(reqStatus) {
    const data = {
      reqId: this.route.snapshot.params.requestId,
      reqStatus: reqStatus,
      userEmail: this.userService.getEmail(),
      noteDate: new Date(),
      noteName: this.requestObj.reqNumber,
      noteDesc: this.landForm.value.itemDescription
    };

    this.userService.resolveRequest(data).subscribe(
      (dataa: any) => {

        this.toastService.success('', 'Вие успешно редактирахте заявката!');
        this.router.navigate([`review-requests`]);
      },
      err => {
      }
    );
  }

  bookDate() {
    this.router.navigate(['calendar/BG-23']);
  }

}
