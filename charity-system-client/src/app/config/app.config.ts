export class AppConfig {
    readonly apiUrl: string;
    readonly jwtIssuer: string;

    constructor() {
        this.apiUrl = 'http://localhost:5500';
        this.jwtIssuer = 'root';
    }
}
