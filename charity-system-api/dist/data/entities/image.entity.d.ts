import { Residence } from './residence.entity';
import { Land } from './land.entity';
export declare class Image {
    id: string;
    fileName: string;
    filePath: string;
    creationDate: Date;
    residenceImage: Promise<Residence>;
    landImage: Promise<Land>;
}
