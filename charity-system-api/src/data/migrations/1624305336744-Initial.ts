import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1624305336744 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `event` (`eventId` varchar(255) NOT NULL, `eventName` varchar(255) NOT NULL, `eventStart` datetime NOT NULL, `eventEnd` datetime NOT NULL, `eventColor` varchar(255) NOT NULL, `eventPhoneNumber` varchar(255) NOT NULL, `eventRegionRegionId` varchar(255) NULL, PRIMARY KEY (`eventId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `regions` (`regionId` varchar(255) NOT NULL, `regionName` varchar(255) NOT NULL, PRIMARY KEY (`regionId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `notification` (`notificationId` varchar(255) NOT NULL, `notificationName` varchar(255) NOT NULL, `notificationMessage` varchar(255) NOT NULL, `notificationDate` datetime NOT NULL, `userNotifyUserId` varchar(255) NULL, PRIMARY KEY (`notificationId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `sertifications` (`sertificationId` varchar(255) NOT NULL, `sertificationName` varchar(255) NOT NULL, `sertificationDate` datetime NOT NULL, `sertificationCampaignId` varchar(255) NOT NULL, `sertificationImageId` varchar(255) NULL, `userUserId` varchar(255) NULL, UNIQUE INDEX `REL_897ca160d055e9910c8bd79f5c` (`sertificationImageId`), PRIMARY KEY (`sertificationId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `items` (`itemId` varchar(255) NOT NULL, `itemName` varchar(255) NOT NULL, `itemDescription` varchar(255) NOT NULL, `itemCategory` enum ('Дрехи', 'Играчки', 'Храна', 'Учебници и книги', 'Домашни потребности', 'Уреди') NOT NULL, `itemPoints` int NOT NULL DEFAULT 0, `itemQuantity` int NOT NULL DEFAULT 0, `itemRequestRequestId` varchar(255) NULL, PRIMARY KEY (`itemId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `request` (`requestId` varchar(255) NOT NULL, `requestNumber` varchar(255) NOT NULL, `requestDate` datetime NOT NULL, `requestStatus` varchar(255) NOT NULL, `requestCampaignID` varchar(255) NOT NULL, `requestAuthorUserId` varchar(255) NULL, PRIMARY KEY (`requestId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`userId` varchar(255) NOT NULL, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `role` enum ('ADMIN', 'USER', 'PAID_USER_3', 'PAID_USER_6', 'PAID_USER_12') NOT NULL DEFAULT 'USER', `points` int NOT NULL DEFAULT 0, `totalItemsDonated` int NOT NULL DEFAULT 0, `totalMoneyDonated` int NOT NULL DEFAULT 0, `isLogged` tinyint NOT NULL DEFAULT 0, `isOkayWithTermsAndConditions` tinyint NOT NULL DEFAULT 0, UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`userId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `campaigns` (`campaignId` varchar(255) NOT NULL, `campaignName` varchar(255) NOT NULL, `campaignDescription` varchar(255) NOT NULL, `campaignStartDate` datetime NOT NULL, `campaignEndDate` datetime NOT NULL, `campaignCollectedMoney` int NOT NULL DEFAULT 0, `phoneForContacts` varchar(255) NOT NULL, `emailForContacts` varchar(255) NOT NULL, `campaignStatus` enum ('Отворена', 'Затворена') NOT NULL DEFAULT 'Отворена', `campaignWinners` text NOT NULL, PRIMARY KEY (`campaignId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `images` (`id` varchar(255) NOT NULL, `fileName` varchar(255) NOT NULL, `filePath` varchar(255) NOT NULL, `creationDate` datetime NOT NULL, `campaignImageCampaignId` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `centers` (`centerId` varchar(255) NOT NULL, `centerName` varchar(255) NOT NULL, `centerAddress` varchar(255) NOT NULL, `centerLink` varchar(255) NOT NULL, `centerImageId` varchar(255) NULL, `centerAreaAreaId` varchar(255) NULL, UNIQUE INDEX `REL_a049a3945251606279f3a39e34` (`centerImageId`), PRIMARY KEY (`centerId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `areas` (`areaId` varchar(255) NOT NULL, `areaName` varchar(255) NOT NULL, `regionRegionId` varchar(255) NULL, PRIMARY KEY (`areaId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `campaigns_campaign_users_users` (`campaignsCampaignId` varchar(255) NOT NULL, `usersUserId` varchar(255) NOT NULL, PRIMARY KEY (`campaignsCampaignId`, `usersUserId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `event` ADD CONSTRAINT `FK_73a98a8fdb489a7cf3ba34b59bc` FOREIGN KEY (`eventRegionRegionId`) REFERENCES `regions`(`regionId`)");
        await queryRunner.query("ALTER TABLE `notification` ADD CONSTRAINT `FK_9ce6f7a29235613687ca5b24558` FOREIGN KEY (`userNotifyUserId`) REFERENCES `users`(`userId`)");
        await queryRunner.query("ALTER TABLE `sertifications` ADD CONSTRAINT `FK_897ca160d055e9910c8bd79f5c7` FOREIGN KEY (`sertificationImageId`) REFERENCES `images`(`id`)");
        await queryRunner.query("ALTER TABLE `sertifications` ADD CONSTRAINT `FK_4fc2ed51fb2e178020e04630c2c` FOREIGN KEY (`userUserId`) REFERENCES `users`(`userId`)");
        await queryRunner.query("ALTER TABLE `items` ADD CONSTRAINT `FK_f6c37818d7a1e40d4f7621b261a` FOREIGN KEY (`itemRequestRequestId`) REFERENCES `request`(`requestId`)");
        await queryRunner.query("ALTER TABLE `request` ADD CONSTRAINT `FK_895e3e9a490643e101d6d0bde2a` FOREIGN KEY (`requestAuthorUserId`) REFERENCES `users`(`userId`)");
        await queryRunner.query("ALTER TABLE `images` ADD CONSTRAINT `FK_08c16e30c020f6e644d86d9f692` FOREIGN KEY (`campaignImageCampaignId`) REFERENCES `campaigns`(`campaignId`)");
        await queryRunner.query("ALTER TABLE `centers` ADD CONSTRAINT `FK_a049a3945251606279f3a39e344` FOREIGN KEY (`centerImageId`) REFERENCES `images`(`id`)");
        await queryRunner.query("ALTER TABLE `centers` ADD CONSTRAINT `FK_3966f67d790881105afaad48964` FOREIGN KEY (`centerAreaAreaId`) REFERENCES `areas`(`areaId`)");
        await queryRunner.query("ALTER TABLE `areas` ADD CONSTRAINT `FK_d3d55ca6dfa50d857d76b7a20e2` FOREIGN KEY (`regionRegionId`) REFERENCES `regions`(`regionId`)");
        await queryRunner.query("ALTER TABLE `campaigns_campaign_users_users` ADD CONSTRAINT `FK_cd8ba11a9e28e73d58e0b776f94` FOREIGN KEY (`campaignsCampaignId`) REFERENCES `campaigns`(`campaignId`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `campaigns_campaign_users_users` ADD CONSTRAINT `FK_f8d7823e1f063bb4b2f908e0245` FOREIGN KEY (`usersUserId`) REFERENCES `users`(`userId`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `campaigns_campaign_users_users` DROP FOREIGN KEY `FK_f8d7823e1f063bb4b2f908e0245`");
        await queryRunner.query("ALTER TABLE `campaigns_campaign_users_users` DROP FOREIGN KEY `FK_cd8ba11a9e28e73d58e0b776f94`");
        await queryRunner.query("ALTER TABLE `areas` DROP FOREIGN KEY `FK_d3d55ca6dfa50d857d76b7a20e2`");
        await queryRunner.query("ALTER TABLE `centers` DROP FOREIGN KEY `FK_3966f67d790881105afaad48964`");
        await queryRunner.query("ALTER TABLE `centers` DROP FOREIGN KEY `FK_a049a3945251606279f3a39e344`");
        await queryRunner.query("ALTER TABLE `images` DROP FOREIGN KEY `FK_08c16e30c020f6e644d86d9f692`");
        await queryRunner.query("ALTER TABLE `request` DROP FOREIGN KEY `FK_895e3e9a490643e101d6d0bde2a`");
        await queryRunner.query("ALTER TABLE `items` DROP FOREIGN KEY `FK_f6c37818d7a1e40d4f7621b261a`");
        await queryRunner.query("ALTER TABLE `sertifications` DROP FOREIGN KEY `FK_4fc2ed51fb2e178020e04630c2c`");
        await queryRunner.query("ALTER TABLE `sertifications` DROP FOREIGN KEY `FK_897ca160d055e9910c8bd79f5c7`");
        await queryRunner.query("ALTER TABLE `notification` DROP FOREIGN KEY `FK_9ce6f7a29235613687ca5b24558`");
        await queryRunner.query("ALTER TABLE `event` DROP FOREIGN KEY `FK_73a98a8fdb489a7cf3ba34b59bc`");
        await queryRunner.query("DROP TABLE `campaigns_campaign_users_users`");
        await queryRunner.query("DROP TABLE `areas`");
        await queryRunner.query("DROP INDEX `REL_a049a3945251606279f3a39e34` ON `centers`");
        await queryRunner.query("DROP TABLE `centers`");
        await queryRunner.query("DROP TABLE `images`");
        await queryRunner.query("DROP TABLE `campaigns`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `request`");
        await queryRunner.query("DROP TABLE `items`");
        await queryRunner.query("DROP INDEX `REL_897ca160d055e9910c8bd79f5c` ON `sertifications`");
        await queryRunner.query("DROP TABLE `sertifications`");
        await queryRunner.query("DROP TABLE `notification`");
        await queryRunner.query("DROP TABLE `regions`");
        await queryRunner.query("DROP TABLE `event`");
    }

}
