import {
  Injectable,
  CanActivate,
  ExecutionContext,
} from '@nestjs/common';
import { Role } from 'src/models/enums/roles.enum';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor() {}

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const user = request.user;

    if (user.role === Role.ADMIN) return true;

    return false;
  }
}
