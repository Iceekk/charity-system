import { Area } from './area.entity';
export declare class Region {
    regionId: string;
    regionName: string;
    areas: Area[];
}
