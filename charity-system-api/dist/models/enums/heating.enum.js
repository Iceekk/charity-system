"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Heating;
(function (Heating) {
    Heating["TEC"] = "\u0422\u0415\u0426";
    Heating["GAS"] = "\u0433\u0430\u0437";
    Heating["FIREPLACE"] = "\u043A\u0430\u043C\u0438\u043D\u0430";
    Heating["HEATPUMP"] = "\u0442\u0435\u0440\u043C\u043E\u043F\u043E\u043C\u043F\u0430";
    Heating["SOLARPANEL"] = "\u0441\u043E\u043B\u0430\u0440\u0435\u043D \u043F\u0430\u043D\u0435\u043B";
    Heating["ELECTRICITY"] = "\u0435\u043B\u0435\u0442\u0440\u0438\u0447\u0435\u0441\u0442\u0432\u043E";
    Heating["OTHER"] = "\u0434\u0440\u0443\u0433\u043E";
})(Heating = exports.Heating || (exports.Heating = {}));
//# sourceMappingURL=heating.enum.js.map