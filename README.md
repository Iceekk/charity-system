A web platform with the primary goal of helping people in need by encouraging people who have a surplus to donate. Donate to make someone happy!
