import { AuthService } from './auth.service';
import { UserRegisterDTO } from '../models/users/user-register.dto';
import { User } from '../data/entities/user.entity';
import { UserLoginDTO } from '../models/users/user-login.dto';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    root(): string;
    sign(user: UserLoginDTO): Promise<object>;
    registerUser(user: UserRegisterDTO): Promise<User>;
}
