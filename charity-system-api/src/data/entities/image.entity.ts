import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToOne,
} from 'typeorm';
import { Center } from './center.entity';
import { Sertification } from './sertification.entity';
import { Campaign } from './campaign.entity';

@Entity({
  name: 'images',
})
export class Image {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  fileName: string;

  @Column()
  filePath: string;

  @Column()
  creationDate: Date;

  @ManyToOne(type => Campaign, campaign => campaign.campaignImages)
  campaignImage: Promise<Campaign>;
}
