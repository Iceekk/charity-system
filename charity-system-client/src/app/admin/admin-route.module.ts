import { EditCampaignComponent } from './edit-campaign/edit-campaign.component';
import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuard } from '../shared/authorization/admin-guard.service';
import { AddCampaignComponent } from './add-campaign/add-campaign.component';
import { AddCenterComponent } from './add-center/add-center.component';
import { AddItemComponent } from './add-item/add-item.component';
import { EditCenterComponent } from './edit-center/edit-center.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        redirectTo: 'add-residence',
        pathMatch: 'full'
      },
      {
        path: 'add-campaign',
        component: AddCampaignComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'add-center',
        component: AddCenterComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'edit-center/:centerId',
        component: EditCenterComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'edit-campaign/:campaignId',
        component: EditCampaignComponent,
        canActivate: [AdminGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
