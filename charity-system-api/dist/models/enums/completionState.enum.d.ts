export declare enum CompletionState {
    INPROJECT = "\u0432 \u043F\u0440\u043E\u0435\u043A\u0442",
    LUXURY = "\u043B\u0443\u043A\u0441",
    WITH_FINISHING_WORKS = "\u0441 \u0434\u043E\u0432\u044A\u0440\u0448\u0438\u0442\u0435\u043B\u043D\u0438 \u0440\u0430\u0431\u043E\u0442\u0438",
    WITHOUT_FINISHING_WORKS = "\u0431\u0435\u0437 \u0434\u043E\u0432\u044A\u0440\u0448\u0438\u0442\u0435\u043B\u043D\u0438 \u0440\u0430\u0431\u043E\u0442\u0438",
    ACT15 = "\u0430\u043A\u0442 15",
    ACT16 = "\u0430\u043A\u0442 16"
}
