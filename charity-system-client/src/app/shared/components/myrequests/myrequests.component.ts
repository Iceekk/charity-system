import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-myrequests',
  templateUrl: './myrequests.component.html',
  styleUrls: ['./myrequests.component.css']
})
export class MyrequestsComponent implements OnInit {

  isAuth: any = false;
  userRequests = [];
  filtersLoaded: Promise<boolean>;


  constructor(
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.isAuth = this.userService.getToken() ? true : false;

    // should get all the requests
    
    this.userService.getUserRequests({userEmail: this.userService.getEmail()}).subscribe(
      (dataa: any) => {
        this.userRequests = dataa.userRequests.map(el => {
          el.requestDate = el.requestDate.substring(0,10);
          return el;
        });
        this.filtersLoaded = Promise.resolve(true);
      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );
  }

  confirmAdd() {
    this.router.navigate(['make-request']);
  }

  confirmAdd2() {
    this.router.navigate(['calendar/BG-23']);
  }

  viewReq(reqId) {
    this.router.navigate([`requests/${reqId}`]);
  }

}
