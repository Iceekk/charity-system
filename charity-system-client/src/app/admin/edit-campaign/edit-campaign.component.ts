import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppConfig } from 'src/app/config/app.config';
import { UserService } from 'src/app/shared/services/user.service';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-edit-campaign',
  templateUrl: './edit-campaign.component.html',
  styleUrls: ['./edit-campaign.component.css']
})
export class EditCampaignComponent implements OnInit {

  public landForm: FormGroup;
  fileArr = [];
  imgArr = [];
  fileObj = [];
  form: FormGroup;
  msg: string;
  progress = 0;
  landImages = [];

  // ===========================================
  dropdownCampaignStatus = [];
  dropdownCampaignStatusSettigns = {};

  selectedCampaignStatus: any;
  campaign: any = {};
  user = {
    isAdmin: false
  };

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private adminService: AdminService,
    private toastService: ToastrService,
    private router: Router,
    private appConfig: AppConfig,
    private formbBuilder: FormBuilder,
    private sanitizer: DomSanitizer
  ) { 
    this.form = this.formbBuilder.group({
      avatar: [null]
    });
  }

  ngOnInit() {

    this.user.isAdmin = this.userService.getRole() === 'ADMIN' ? true : false;

    this.dropdownCampaignStatus = [
      { item_id: 'OPEN', item_text: 'Отворена' },
      { item_id: 'CLOSED', item_text: 'Затворена' }
    ];
    this.dropdownCampaignStatusSettigns = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 3
    };
    
    this.userService.getSpecificCampaign({campaignId: this.route.snapshot.params.campaignId}).subscribe((data: any) => {
      
      this.campaign = data;
      this.campaign.campaignStartDate = this.campaign.campaignStartDate.substring(0,10);
      this.campaign.campaignEndDate = this.campaign.campaignEndDate.substring(0,10);
      
      this.landForm.patchValue({
        campaignDescription: this.campaign.campaignDescription,
        campaignPhone: this.campaign.phoneForContacts,
        campaignEmail: this.campaign.emailForContacts
      });

      this.selectedCampaignStatus = [{item_id: "OPEN", item_text: "Отворена"}];
      
    }, (err) => {
      
    });

    this.buildTheForm();
  }

  onCampaignStatusSelect(item: any) {

  }

  successToast() {
    this.toastService.success('', 'Admin added!', { timeOut: 1000 });
  }

  errToast() {
    this.toastService.error('Please try again', 'Wrong input!', {
      timeOut: 1500
    });
  }

  getErrorMessage() {
    return '';
  }

  public buildTheForm() {
    this.landForm = this.formbBuilder.group({
      campaignDescription: this.formbBuilder.control(this.campaign.campaignDescription),
      campaignPhone: this.formbBuilder.control(''),
      campaignEmail: this.formbBuilder.control('')
    });
  }

  get formData() {
    return this.landForm.value;
  }

  confirmAdd(addAdminData: any): void {
    const data = {
      campaignId: this.route.snapshot.params.campaignId,
      moreData: addAdminData,
      campaignStatus: this.selectedCampaignStatus,
      landImages: this.landImages
    };

    this.selectedCampaignStatus = null;
    this.landForm.reset();
    this.fileArr = [];
    this.landImages = [];

    this.userService.editSpecificCampaign(data).subscribe((data: any) => {
        this.toastService.success('', 'Вие успешно редактирахте кампанията!');

        this.router.navigate([`/campaigns/${this.campaign.campaignId}`]);
      },
      err => {
        this.toastService.error('', 'Възникна грешка при редактирането на кампанията!');
      }
    );
  }

  upload(e: any) {
    const fileListAsArray = Array.from(e);
    fileListAsArray.forEach((item, i) => {
      const file = e as HTMLInputElement;
      const url = URL.createObjectURL(file[i]);
      this.imgArr.push(url);
      this.fileArr.push({ item, url });
    });

    this.fileArr.forEach(item => {
      this.fileObj.push(item.item);
    });

    // Set files form control
    this.form.patchValue({
      avatar: this.fileObj
    });

    this.form.get('avatar').updateValueAndValidity();

    // Upload to server
    this.adminService.addFiles(this.form.value.avatar).subscribe(
      data => {
        this.landImages = data.images;
      },
      error => {
        // console.log(error);
      }
    );
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

}
