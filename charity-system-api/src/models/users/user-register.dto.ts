import {
  IsString,
  Matches,
  IsEmail,
  IsBoolean,
} from 'class-validator';
import { Optional } from '@nestjs/common';
import { Transform } from 'class-transformer';

export class UserRegisterDTO {
  @IsEmail()
  email: string;

  @IsString()
  // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  @Transform(value => value)
  password: string;

  @IsBoolean()
  isOkayWithTermsAndConditions: boolean;

  @Optional()
  dateOfPayment: Date;

  @Optional()
  typeOfPayment: string;

  @Optional()
  role: string;
}
