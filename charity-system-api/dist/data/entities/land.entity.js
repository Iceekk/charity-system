"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const area_entity_1 = require("./area.entity");
const company_entity_1 = require("./company.entity");
const searchType_enum_1 = require("./../../models/enums/searchType.enum");
const currencyType_enum_1 = require("./../../models/enums/currencyType.enum");
const estateType_enum_1 = require("./../../models/enums/estateType.enum");
const image_entity_1 = require("./image.entity");
let Land = class Land {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Land.prototype, "landId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Land.prototype, "landName", void 0);
__decorate([
    typeorm_1.Column({ default: '' }),
    __metadata("design:type", String)
], Land.prototype, "landDescription", void 0);
__decorate([
    typeorm_1.Column({ enum: [searchType_enum_1.SearchType.SALES, searchType_enum_1.SearchType.RENTS], type: 'enum' }),
    __metadata("design:type", String)
], Land.prototype, "landType", void 0);
__decorate([
    typeorm_1.Column({ default: '' }),
    __metadata("design:type", String)
], Land.prototype, "landNeighborhood", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Land.prototype, "landPrice", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Land.prototype, "landQuadrature", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], Land.prototype, "landViews", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], Land.prototype, "reportsCounter", void 0);
__decorate([
    typeorm_1.Column({ enum: [
            currencyType_enum_1.CurrencyType.BGN,
            currencyType_enum_1.CurrencyType.EUR
        ], type: 'enum' }),
    __metadata("design:type", String)
], Land.prototype, "currencyType", void 0);
__decorate([
    typeorm_1.Column({ enum: [
            estateType_enum_1.EstateType.ONEBEDROOM,
            estateType_enum_1.EstateType.TWOBEDROOM,
            estateType_enum_1.EstateType.THREEBEDROOM,
            estateType_enum_1.EstateType.INDUSTRIAL_ROOM,
            estateType_enum_1.EstateType.MULTIROOM,
            estateType_enum_1.EstateType.MAISONETTE,
            estateType_enum_1.EstateType.OFFICE,
            estateType_enum_1.EstateType.HOUSE,
            estateType_enum_1.EstateType.FLOOR,
            estateType_enum_1.EstateType.VILLA,
            estateType_enum_1.EstateType.WAREHOUSE,
            estateType_enum_1.EstateType.AGRICULTURAL_PROPERTY,
            estateType_enum_1.EstateType.SHOP,
            estateType_enum_1.EstateType.HOTEL,
            estateType_enum_1.EstateType.GARAGE
        ], type: 'enum' }),
    __metadata("design:type", String)
], Land.prototype, "landEstateType", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Land.prototype, "landFeatures", void 0);
__decorate([
    typeorm_1.Column({ default: true }),
    __metadata("design:type", Boolean)
], Land.prototype, "isActive", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", Date)
], Land.prototype, "dateOfCreation", void 0);
__decorate([
    typeorm_1.ManyToOne(type => area_entity_1.Area, area => area.areaLands),
    __metadata("design:type", Promise)
], Land.prototype, "area", void 0);
__decorate([
    typeorm_1.ManyToOne(type => company_entity_1.Company, company => company.companyLands),
    __metadata("design:type", Promise)
], Land.prototype, "company", void 0);
__decorate([
    typeorm_1.OneToMany(type => image_entity_1.Image, image => image.landImage, { eager: true }),
    __metadata("design:type", Array)
], Land.prototype, "landImages", void 0);
Land = __decorate([
    typeorm_1.Entity({
        name: 'lands',
    })
], Land);
exports.Land = Land;
//# sourceMappingURL=land.entity.js.map