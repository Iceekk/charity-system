import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { UserService } from './shared/services/user.service';
import { Role } from './shared/models/enums/roles.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'imotino';

  options: FormGroup;
  public loginForm: FormGroup;
  public registerForm: FormGroup;

  isAuth: any = false; // should be FALSE !!!
  isAdmin: any = false; // should be FALSE !!!
  hasNotifications: any = false; // should be FALSE!!!
  hasMessages: any = false; // should be FALSE!!!
  loading = false;

  public genericErrorMsg = 'Това поле е задължително!';
  public genMinLengthMsg = 'Минималната дължина е 8 символа!';
  public emailErrMsg = 'Имейла е невалиден!';
  public passErrMsg = 'Паролата трябва да съдържа главна буква, малка буква, цифра и спеациален символ!';
  public genMaxLengthMsg = 'Максималната дължина трябва да е под 50 символа!';
  public passwordPattern = ('([A-Za-z0-9@#$%&*]+)$');

  constructor(
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.buildLoginForm();
    this.buildRegisterForm();

    this.isAdmin = this.userService.getRole() === 'ADMIN' ? true : false;
    this.isAuth = this.userService.getToken() ? true : false;

    this.userService.hasRequestsForReview().subscribe(
      (dataa: any) => {
        this.hasNotifications = dataa.result;
      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );

    this.userService.hasMessagesForReview({userEmail: this.userService.getEmail()}).subscribe(
      (dataa: any) => {
        this.hasMessages = dataa.result;
      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );
  }

  public buildLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      email: [null,
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(10),
          Validators.maxLength(50),
        ])],
      password: [null,
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(50),
          // Validators.pattern(this.passwordPattern)
        ])],
    });
  }

  public buildRegisterForm(): void {
    this.registerForm = this.formBuilder.group({
      emailReg: [null,
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(10),
          Validators.maxLength(50),
        ])],
      passwordReg: [null,
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(50),
          // Validators.pattern(this.passwordPattern)
        ])],
      repeatPasswordReg: [null,
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(50),
          // Validators.pattern(this.passwordPattern)
        ])],
    });
  }

  successToast() {
    this.toastService.success('', 'Успешно влизане!', { timeOut: 3000 });
  }

  errToast() {
    this.toastService.error('Моля, опитайте отново', 'Грешни данни!', { timeOut: 1500 });
    this.loginForm.controls.password.setErrors(null);
  }

  getEmailErrorMessage() {
    return '';
  }

  getPassErrorMessage() {
    return '';
  }

  getEmailErrorMessageReg() {
    return '';
  }

  getPassErrorMessageReg() {
    return '';
  }

  register(): void {
    this.userService.registerUser(this.registerForm.value).subscribe((data) => {

      this.toastService.success('', 'Успешна регистрация!');

    }, (err) => {
      this.toastService.error('', 'Вече има съществуващ потребител с въведените данни!');
    });
  }

  login(): void {

    this.loading = true;

    const user = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    };


    if (!user.email || !user.password) {
      this.errToast();
      this.loginForm.reset();
      return;
    }

    this.userService.login(user)
      .subscribe((data: { message: string, token: string }) => {
        localStorage.setItem('token', data.token);

        const role = this.userService.getRole();

        if (role === Role.ADMIN) {
          this.loading = false;
          this.isAuth = true;
          this.isAdmin = true;
          this.successToast();
          this.loginForm.reset();
          this.router.navigate(['']);

        } else if (role === Role.USER) {

          this.loading = false;
          this.isAuth = true;
          this.successToast();
          this.loginForm.reset();
          this.router.navigate(['']);
        }

      }, (err: HttpErrorResponse) => {
        this.errToast();
        this.loginForm.reset();
      });
  }

  logout(): void {

    this.userService.logout(this.userService.getEmail()).subscribe((data) => {

      this.toastService.success(`Успешно излизане!`);
      this.isAuth = false;
      this.isAdmin = false;
      this.hasNotifications = false;
      this.router.navigate(['']);
      localStorage.removeItem('token');
      localStorage.clear();
    }, (err) => {

    });

  }

}
