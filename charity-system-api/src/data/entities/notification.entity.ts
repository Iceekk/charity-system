import { User } from './user.entity';
import { Region } from './region.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Center } from './center.entity';

@Entity({
  name: 'notification',
})
export class Notification {
  @PrimaryGeneratedColumn('uuid')
  notificationId: string;

  @Column()
  notificationName: string;

  @Column()
  notificationMessage: string;

  @Column()
  notificationDate: Date;

  @ManyToOne(type => User, user => user.userNotifications)
  userNotify: Promise<User>;

}
