import { ChangeDetectorRef, Component, ElementRef, OnInit } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import {HostListener, ViewChild} from '@angular/core';
import { MdbTableDirective, MdbTablePaginationComponent } from 'angular-bootstrap-md';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild('row', { static: true }) row: ElementRef;

  elements: any = [];
  headElements = ['Date', 'Name', 'Amount', 'Transaction Number', 'Receiver'];

  searchText: string = '';
  previous: string;

  maxVisibleItems: number = 15;
  
  
  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private toastService: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute,
  ) { 
  }

  @HostListener('input') oninput() {
    this.mdbTablePagination.searchText = this.searchText;
  }
  
  ngOnInit() {

    this.userService.getAllTransactions().subscribe(
      (dataa: any) => {
        // console.log(dataa);

        this.elements = dataa;
        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
        
      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );
    // ===========================================================================================================

    this.userService.getPieData().subscribe(
      (dataa: any) => {
        // console.log(dataa);

        let chart: any = am4core.create("chartdiv", am4charts.PieChart);
        chart.data = dataa;

        let pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "litres";
        pieSeries.dataFields.category = "country";
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeWidth = 3;
        pieSeries.slices.template.strokeOpacity = 1;
      
        // This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;
        
      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );

    // ============================================================================================================

    this.userService.getXYData().subscribe(
      (dataa: any) => {
        // console.log(dataa);

        let chart1 = am4core.create("chartdiv1", am4charts.XYChart);
        chart1.dateFormatter.dateFormat = "MMM YYYY";
        chart1.numberFormatter.numberFormat = "#.#a";
        chart1.numberFormatter.bigNumberPrefixes = [
          { "number": 1e+3, "suffix": "K" },
          { "number": 1e+6, "suffix": "M" },
          { "number": 1e+9, "suffix": "B" }
        ];
        
        // Chart title
        let title = chart1.titles.create();
        title.text = "Брой дарители през времето:";
        title.fontSize = 26;
        title.fontWeight = "bold";
        title.paddingBottom = 10;
        
        // Add data
        chart1.data = dataa;
    
        // Create axes
        let dateAxis = chart1.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.grid.template.location = 0;
        dateAxis.renderer.minGridDistance = 30;
    
        let valueAxis = chart1.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.inside = true;
        valueAxis.renderer.labels.template.verticalCenter = "bottom";
        valueAxis.renderer.labels.template.dx = -5;
        valueAxis.renderer.labels.template.dy = 10;
        valueAxis.renderer.maxLabelPosition = 0.95;
        valueAxis.title.text = "Брой дарители";
        valueAxis.title.marginRight = 5;
    
        // Create series
        function createSeries(field, name, color, dashed) {
          let series = chart1.series.push(new am4charts.LineSeries());
          series.dataFields.valueY = field;
          series.dataFields.dateX = "date";
          series.name = name;
          series.tooltipText = "[bold]{name}[/]\n{dateX}: [b]{valueY}[/]";
          series.strokeWidth = 2;
          series.smoothing = "monotoneX";
          series.stroke = color;
          
          if (dashed) {
            series.strokeDasharray = "5 3";
          }
          
          return series;
        }
    
        createSeries("observed", "Дарители", am4core.color("#0C3C55"), true);
    
        chart1.legend = new am4charts.Legend();
        chart1.cursor = new am4charts.XYCursor();

      },
      err => {
        // this.toastService.error('', 'Възникна грешка при създаването на нова заявка!');
      }
    );

  }

  ngAfterViewInit() {
    //=======================================================================================
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(8);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  addNewRow() {
    this.mdbTable.addRow({
      place: this.elements.length.toString(),
      name: 'Wpis ' + this.elements.length,
      email: 'Last ' + this.elements.length,
      points: 'Handle ' + this.elements.length
    });
    this.emitDataSourceChange();
  }

  addNewRowAfter() {
    this.mdbTable.addRowAfter(1, {place: 3, name: 'Nowy', email: 'Row', points: 2});
    this.mdbTable.getDataSource().forEach((el: any, index: any) => {
      el.points = (index + 1).toString();
    });
    this.emitDataSourceChange();
  }

  removeLastRow() {
    this.mdbTable.removeLastRow();
    this.emitDataSourceChange();
    this.mdbTable.rowRemoved().subscribe((data: any) => {
      // console.log(data);
    });
  }

  removeRow() {
    this.mdbTable.removeRow(1);
    this.mdbTable.getDataSource().forEach((el: any, index: any) => {
      el.id = (index + 1).toString();
    });
    this.emitDataSourceChange();
    this.mdbTable.rowRemoved().subscribe((data: any) => {
      console.log(data);
    });
  }

  emitDataSourceChange() {
    this.mdbTable.dataSourceChange().subscribe((data: any) => {
      console.log(data);
    });
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

}
