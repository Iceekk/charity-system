"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const residence_entity_1 = require("./residence.entity");
const land_entity_1 = require("./land.entity");
let Company = class Company {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Company.prototype, "companyId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Company.prototype, "companyName", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Company.prototype, "companyPhone", void 0);
__decorate([
    typeorm_1.Column({ unique: true }),
    __metadata("design:type", String)
], Company.prototype, "companyEmail", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Company.prototype, "companySiteURL", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Company.prototype, "companyAddress", void 0);
__decorate([
    typeorm_1.OneToMany(type => residence_entity_1.Residence, residence => residence.company, { eager: true }),
    __metadata("design:type", Promise)
], Company.prototype, "companyResidences", void 0);
__decorate([
    typeorm_1.OneToMany(type => land_entity_1.Land, land => land.company, { eager: true }),
    __metadata("design:type", Promise)
], Company.prototype, "companyLands", void 0);
Company = __decorate([
    typeorm_1.Entity({
        name: 'companies',
    })
], Company);
exports.Company = Company;
//# sourceMappingURL=company.entity.js.map