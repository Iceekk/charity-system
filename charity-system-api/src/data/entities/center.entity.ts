import { Image } from './image.entity';
import { Area } from './area.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity({
  name: 'centers',
})
export class Center {
  @PrimaryGeneratedColumn('uuid')
  centerId: string;

  @Column()
  centerName: string;

  @Column()
  centerAddress: string;

  @Column()
  centerLink: string;

  @OneToOne(() => Image)
  @JoinColumn()
  centerImage: Image;

  @ManyToOne(type => Area, area => area.areaCenters)
  centerArea: Promise<Area>;
}
