"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PaymentTypeEnum;
(function (PaymentTypeEnum) {
    PaymentTypeEnum["NOT_PAID"] = "NOT_PAID";
    PaymentTypeEnum["PAID_1"] = "PAID_1";
    PaymentTypeEnum["PAID_3"] = "PAID_3";
    PaymentTypeEnum["PAID_6"] = "PAID_6";
    PaymentTypeEnum["PAID_12"] = "PAID_12";
})(PaymentTypeEnum = exports.PaymentTypeEnum || (exports.PaymentTypeEnum = {}));
//# sourceMappingURL=paymentType.enum.js.map