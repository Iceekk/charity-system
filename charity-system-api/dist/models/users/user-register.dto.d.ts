export declare class UserRegisterDTO {
    email: string;
    password: string;
    dateOfPayment: Date;
    typeOfPayment: string;
    role: string;
}
