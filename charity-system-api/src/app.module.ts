import { Module, HttpModule } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [ConfigModule, HttpModule, AuthModule, DatabaseModule, UsersModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
