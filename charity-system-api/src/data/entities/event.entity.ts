import { Region } from './region.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity({
  name: 'event',
})
export class Event {
  @PrimaryGeneratedColumn('uuid')
  eventId: string;

  @Column()
  eventName: string;

  @Column()
  eventStart: Date;

  @Column()
  eventEnd: Date;

  @Column()
  eventColor: string;

  @Column()
  eventPhoneNumber: string;

  @ManyToOne(type => Region, region => region.events)
  eventRegion: Promise<Region>;

}
