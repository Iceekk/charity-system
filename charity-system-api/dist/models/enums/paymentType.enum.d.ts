export declare enum PaymentTypeEnum {
    NOT_PAID = "NOT_PAID",
    PAID_1 = "PAID_1",
    PAID_3 = "PAID_3",
    PAID_6 = "PAID_6",
    PAID_12 = "PAID_12"
}
