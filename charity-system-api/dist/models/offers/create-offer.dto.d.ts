export declare class CreateResidenceDTO {
    name: string;
    description: string;
    type: string;
    neighborhood: string;
    price: number;
    quadrature: number;
    typeOfProperty: string;
    construction: string;
    state: string;
    heating: string;
    floor: string;
    stageOfConstruction: string;
    isActive: boolean;
    images: string;
}
