export enum ItemCategory {
    CLOTHES = 'Дрехи',
    TOYS = 'Играчки',
    FOOD = 'Храна',
    BOOKS = 'Учебници и книги',
    HOUSEHOLDNEEDS = 'Домашни потребности',
    DEVICES = 'Уреди',
}
