import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from './../services/user.service';

@Injectable()
export class RoleGuard implements CanActivate {

    constructor(
        public auth: UserService,
        public router: Router) {
    }

    canActivate(): boolean {
        if (!this.auth.getToken()) {
            return true;
        }

        return true;
    }
}
