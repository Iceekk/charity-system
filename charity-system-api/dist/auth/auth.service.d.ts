import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../models/interfaces/jwt-payload';
import { GetUserDTO } from '../models/users/get-user.dto';
import { UserLoginDTO } from '../models/users/user-login.dto';
import { User } from '../data/entities/user.entity';
import { Repository } from 'typeorm';
import { UserRegisterDTO } from '../models/users/user-register.dto';
export declare class AuthService {
    private readonly usersRepository;
    private readonly jwtService;
    constructor(usersRepository: Repository<User>, jwtService: JwtService);
    signIn(user: UserLoginDTO): Promise<string>;
    validateUser(payload: JwtPayload): Promise<GetUserDTO>;
    registerUser(user: UserRegisterDTO): Promise<User>;
}
