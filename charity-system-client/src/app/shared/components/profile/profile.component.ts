import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/admin/admin.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  sertifications: any = [];
  fileArr = [];
  imgArr = [];
  fileObj = [];
  form: FormGroup;
  msg: string;
  progress = 0;
  landImages = [];
  user: any = {};
  isAdmin: any = false;
  itemsQuantity: any;
  dropdownCategory = [];
  dropdownCategorySettigns = {};

  selectedCategory: any;
  userItems: any = {};

  constructor(
    private toastService: ToastrService,
    private formbBuilder: FormBuilder,
    private adminService: AdminService,
    private userService: UserService,
    private sanitizer: DomSanitizer,
    private route: ActivatedRoute
  ) {
    this.form = this.formbBuilder.group({
      avatar: [null]
    });
  }

  ngOnInit() {
    this.isAdmin = this.userService.getRole() === 'ADMIN' ? true : false;

    this.dropdownCategory = [
      { item_id: 'CLOTHES', item_text: 'Дрехи' },
      { item_id: 'TOYS', item_text: 'Играчки' },
      { item_id: 'FOOD', item_text: 'Храна' },
      { item_id: 'BOOKS', item_text: 'Учебници и книги' },
      { item_id: 'HOUSEHOLDNEEDS', item_text: 'Домашни потребности' },
      { item_id: 'DEVICES', item_text: 'Уреди' }
    ];
    this.dropdownCategorySettigns = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 3
    };

    this.userService.getSpecificUser({userId: this.route.snapshot.params.userId}).subscribe((data: any) => {

      this.sertifications = data.sertifications;
      this.userItems = data.tempItems;
      this.user = data;
    }, (err) => {
    });
  }

  onCategorySelect(item: any) {
  }

  confirmAdd(){

    const data = {
      userId: this.route.snapshot.params.userId,
      image: this.landImages,
      sertDate: new Date(),
    }

    this.userService.addSertificate(data).subscribe((data: any) => {

      this.sertifications.push(data);
    }, (err) => {
      // this.toastService.error('', 'Вече има съществуващ потребител с въведените данни!');
    });

    this.fileArr = [];
    this.landImages = [];
  }

  upload(e: any) {
    const fileListAsArray = Array.from(e);
    fileListAsArray.forEach((item, i) => {
      const file = e as HTMLInputElement;
      const url = URL.createObjectURL(file[i]);
      this.imgArr.push(url);
      this.fileArr.push({ item, url });
    });

    this.fileArr.forEach(item => {
      this.fileObj.push(item.item);
    });

    // Set files form control
    this.form.patchValue({
      avatar: this.fileObj
    });

    this.form.get('avatar').updateValueAndValidity();

    // Upload to server
    this.adminService.addFiles(this.form.value.avatar).subscribe(
      data => {
        this.landImages = data.images;
      },
      error => {
      }
    );
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

}
